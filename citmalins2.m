% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees
%insecticide spraying with two different scenarios plotted together

clear
format long

% Parameter input
par = zeros(1,12);
par(1) = 0.05*365; % a
par(2) = 0.1; % b
par(3) = 0.3; % c
par(4) = 1/60; % r
par(5) = 1/10; % r_1
par(6) = 0.03631705*365; % mu
par(7) = 10/365; % EIP - number of days until psyllid becomes infectious
par(8) = 8000; % lambda
% parameters relating to costs/profits
par(12) = 388; %profit of susceptible tree
par(13) = 0.4; %profit reduction for infection
rep = 50; %cost of replacing a tree
inscost = 30; %cost of 1 day of insecticide
r = 0.001; %discount factor

tau = 0.5; % host time delay
EIP = par(7); % psyllid time delay

% Initial conditions - need history of parameters since we have delay
S0 = 101;
I0 = 0;
R0 = 0;
SV0 = 500;
EV0 = 0;
IV0 = 0;
Prof0 = 0;

uhist = [S0,I0,R0,SV0,EV0,IV0,Prof0]; %initial infection starts exactly at t = 0
                                % and not beforehand

% Time solved for
tspan = [0,20];

options = ddeset('InitialY',[100,1,0,500,0,0,0],'reltol', 1e-8, 'abstol', 1e-6);

for j = 1:2
    if j == 1
        %parameters related to insecticide spray
        par(9) = 0;  % phase for time of year of spray, 0, pi/2, pi, 3pi/2
        par(10) = 20; % m, number of days spraying
        par(11) = 0.97; % rho, effectiveness of spray
        
        % solver
        sol = dde23(@citmalinsectfun,[tau,EIP],uhist,tspan,options,par);

        t1 = sol.x.';
        y1 = sol.y.';
        
    else
        %parameters related to insecticide spray
        par(9) = 3*pi/2; % phase for time of year of spray, 0, pi/2, pi, 3pi/2
        par(10) = 20; % m, number of days spraying
        par(11) = 0.65; % rho, effectiveness of spray
        
        % solver
        sol = dde23(@citmalinsectfun,[tau,EIP],uhist,tspan,options,par);

        t2 = sol.x.';
        y2 = sol.y.';
        
    end
end

%cost of spraying - if number of days spraying changes between runs -
%change par(10) command
crep = exp(-r.*t1).*rep.*y1(:,3);
cins = exp(-r.*t1).*t1.*par(10).*inscost;
income = exp(-r.*t1).*y1(:,7);
prof = income - crep - cins;
A1 = [t1,crep,cins,income,prof];

crep = exp(-r.*t2).*rep.*y2(:,3);
cins = exp(-r.*t2).*t2.*par(10).*inscost;
income = exp(-r.*t2).*y2(:,7);
prof = income - crep - cins;
A2 = [t2,crep,cins,income,prof];

subplot('position',[0.09,0.55,0.43,0.43])
plot(t1,y1(:,1:3),'LineWidth',2)
axis([0 20 0 130])
set(gca,'XTick',[0 5 10 15 20]);
set(gca,'XTicklabel',{'';'';'';'';''});
text(1,120,'A','FontSize',12)
ylabel('Trees')

subplot('position',[0.09,0.1,0.43,0.43])
plot(t1,y1(:,4:6),'LineWidth',2)
axis([0 20 0 600])
xlabel('Years')
ylabel('Psyllids')

subplot('position',[0.55,0.55,0.43,0.43])
plot(t2,y2(:,1:3),'LineWidth',2)
set(gca,'XTick',[0 5 10 15 20]);
set(gca,'XTicklabel',{'';'';'';'';''});
set(gca,'YTick',[0 20 40 60 80 100 120]);
set(gca,'YTicklabel',{'';'';'';'';'';'';''});
axis([0 20 0 130])
text(1,120,'B','FontSize',12)

subplot('position',[0.55,0.1,0.43,0.43])
plot(t2,y2(:,4:6),'LineWidth',2)
axis([0 20 0 600])
set(gca,'YTick',[0 100 200 300 400 500 600]);
set(gca,'YTicklabel',{'';'';'';'';'';'';''});
xlabel('Years')

lab1 = find(y1(:,2)==max(y1(:,2)));
t1(lab1)
max(y1(:,2))

lab2 = find(y2(:,2)==max(y2(:,2)));
t2(lab2)
max(y2(:,2))

