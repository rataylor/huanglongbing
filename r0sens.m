% Local Sensitivity of R0 to changes in each of the parameters over
% temperature.
% Essentially the same as Fig 3 in Mordecai et al 2013, but with parameters
% for HLB model
% Note that currently we only have EFD, pea, MDR and mu fit against
% temperature

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% edits with new mu (added in a very low temperature death rate value)
% which gives new fit for longevity and for muC.
% also switch mdr back to briere fit
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%
% This is for the malaria based model
%%%%%%

clear
%t = 0:0.001:2;
%T = 25.*(1+0.5.*sin(2.*pi.*t));
T = 10:0.001:40;

a = 0.05*365;
b = 0.1;
c = 0.3;
r1 = 1/10; %%%%%% THIS SHOULD BE 1/10!!!!
phi = 36.5; % the same as PDR = 1/EIP implies it takes 10 days for psyllid to become infectious
N = 101;

%muC = 0.03631705*365;
muC = (1/(39.78571))*365; % new average longevity inc cold temp 
peaC = 0.7294;
MDRC = 0.04972*365;
EFDC = 5.306*365;

pea = 0.47192 + 0.0109.*T;
%MDR = -0.00012.*365.*(T - 11.635).*(T - 61.28).*heaviside(T - 11.635);
MDR = 5.286e-05.*365.*T.*(T - 10.02).*sqrt(34.17 - T).*heaviside(T - 10.02).*heaviside(34.17 - T); %MDR as briere
EFD = 0.0107.*365.*T.*(T - 13).*sqrt(30.8 - T).*heaviside(T - 13).*heaviside(30.8 - T);
%mu = (-0.006036 + 0.001233.*T).*365; using longevity table
%mu = (-0.014920 + 0.002173.*T).*365; %Different fit using fig 2 instead
L = -0.14221.*(T.^2) + 4.31998.*T + 31.25498; %longevity with new cold data
mu = (1./L)*365;

R0 = ((EFD.*pea.*MDR.*(a^2)*b*c./(N*r1.*(mu.^3))).*exp(-mu./phi)).^(0.5);

%changes to dMDR and dmu due to above changes of form
dPEA = 0.0109;
dMDR = (5.286e-05*365.*(T-10.02).*sqrt(34.17-T) + 5.286e-05*365.*T.*sqrt(34.17-T) + ...
            - 0.5*5.286e-05*365.*T.*(T-10.02).*(34.17 - T).^(-0.5)).*heaviside(T - 10.02).*heaviside(34.17 - T);
dEFD = (0.0107*365.*(T-13).*sqrt(30.8-T) + 0.0107*365.*T.*sqrt(30.8-T) + ...
            - 0.5*0.0107*365.*T.*(T-13).*(30.8 - T).^(-0.5)).*heaviside(T - 13).*heaviside(30.8 - T);
dmu = 365*(2*0.14221.*T - 4.31998)./((-0.14221.*(T.^2) + 4.31998.*T + 31.25498).^2);

%dR0/R0:
dR0 = (1./(2.*EFD)).*dEFD + (1./(2.*pea)).*dPEA + (1./(2.*MDR)).*dMDR - (3./(2*mu) + 1./(2*phi)).*dmu;

% subplot('position',[0.1 0.1 0.4 0.85])
% plot(T,R0,'b','LineWidth', 2)
% xlabel('Temperature (^{\circ} C)')
% ylabel('R_0')
% subplot('position',[0.55 0.1 0.4 0.85])
% plot(t,R0,'b','LineWidth', 2)
% xlabel('Time (years)')
%ylabel('R_0')

plot(T,dR0,'k','LineWidth',2)
hold on
plot(T,(1./(2.*EFD)).*dEFD,'b','LineWidth',2)
plot(T,(1./(2.*pea)).*dPEA,'r','LineWidth',2)
plot(T,(1./(2.*MDR)).*dMDR,'g','LineWidth',2)
plot(T, -(3./(2*mu) + 1./(2*phi)).*dmu, 'y','LineWidth',2)

figure
plot(T,dR0.*R0,'k','LineWidth',2)
hold on
plot(T,(1./(2.*EFD)).*dEFD.*R0,'b','LineWidth',2)
plot(T,(1./(2.*pea)).*dPEA.*R0,'r','LineWidth',2)
plot(T,(1./(2.*MDR)).*dMDR.*R0,'g','LineWidth',2)
plot(T, -(3./(2*mu) + 1./(2*phi)).*dmu.*R0, 'm','LineWidth',2)
line([0 40],[0 0],'LineStyle','--','Color','k')
axis([13.5 30 -2 2])
legend('R0','EFD','Pea','MDR','\mu','Location','SouthWest')
xlabel('Temperature (\circC)')
ylabel('dR0/dT')

% Add in malaria parameters instead of constants for a, b, c and phi.
% a2 = 0.000203.*365.*T.*(T - 11.7).*sqrt(42.3 - T).*heaviside(T - 11.7).*heaviside(42.3 - T);
% bc = -0.54.*T.^2 + 25.2.*T - 206;
% phi2 = 0.000111.*365.*T.*(T - 14.7).*sqrt(34.4 - T).*heaviside(T - 14.7).*heaviside(34.4 - T);
% 
% R02 = ((EFD.*pea.*MDR.*(a2.^2).*bc./(N*r1.*(mu.^2))).*(3*phi./(3*phi + mu)).^3).^(0.5);
% 
% da = (0.000203*365.*(T-11.7).*sqrt(42.3-T) + 0.000203*365.*T.*sqrt(42.3-T) + ...
%             - 0.5*0.000203*365.*T.*(T-11.7).*(42.3 - T).^(-0.5)).*heaviside(T - 11.7).*heaviside(42.3 - T);
%        
% dbc = -0.54.*2.*T + 25.2;
% dphi = (0.000111*365.*(T-14.7).*sqrt(34.4-T) + 0.000111*365.*T.*sqrt(34.4-T) + ...
%             - 0.5*0.000111*365.*T.*(T-14.7).*(34.4 - T).^(-0.5)).*heaviside(T - 14.7).*heaviside(34.4 - T);
% 
% 
% dR02 = (1./(2.*EFD)).*dEFD + (1./(2.*pea)).*dPEA + (1./(2.*MDR)).*dMDR - (3./(2*mu) + 1./(2*phi)).*dmu + (1./a2).*da +(1./(2*bc)).*dbc + ((3/2).*(mu./phi2).*(1./(3*phi2 + mu))).*dphi;
% 
% figure
% plot(T,dR02,'k','LineWidth',2)
% hold on
% plot(T,(1./(2.*EFD)).*dEFD,'b','LineWidth',2)
% plot(T,(1./(2.*pea)).*dPEA,'r','LineWidth',2)
% plot(T,(1./(2.*MDR)).*dMDR,'g','LineWidth',2)
% plot(T, -(1./mu + 3./(2.*(3*phi + mu))).*dmu, 'y','LineWidth',2)
% plot(T,(1./a2).*da,'c','LineWidth',2)
% plot(T,(1./(2*bc)).*dbc,'m','LineWidth',2)
% plot(T,((3/2).*(mu./phi2).*(1./(3*phi2 + mu))).*dphi,'b--','LineWidth',2)

%R0 with constants instead of variable parameters.
R0mu = ((EFD.*pea.*MDR.*(a^2)*b*c./(N*r1.*(muC.^2))).*(exp(-muC./phi))).^(0.5);
R0EFD = ((EFDC.*pea.*MDR.*(a^2)*b*c./(N*r1.*(mu.^2))).*(exp(-mu./phi))).^(0.5);
R0MDR = ((EFD.*pea.*MDRC.*(a^2)*b*c./(N*r1.*(mu.^2))).*(exp(-mu./phi))).^(0.5);
R0pea = ((EFD.*peaC.*MDR.*(a^2)*b*c./(N*r1.*(mu.^2))).*(exp(-mu./phi))).^(0.5);

figure
plot(T,R0,'k','LineWidth',2)
hold on
plot(T,R0EFD,'b','LineWidth',2)
plot(T,R0pea,'r','LineWidth',2)
plot(T,R0MDR,'g','LineWidth',2)
plot(T, R0mu, 'm','LineWidth',2)
legend('R0','EFD constant','Pea constant','MDR constant','\mu constant')
xlabel('Temperature (\circC)')
ylabel('R0')

figure
subplot('Position',[0.07 0.09 0.42 0.89])
plot(T,R0,'k','LineWidth',2)
hold on
plot(T,R0EFD,'b','LineWidth',2)
plot(T,R0pea,'r','LineWidth',2)
plot(T,R0MDR,'g','LineWidth',2)
plot(T, R0mu, 'm','LineWidth',2)
xlabel('Temperature (\circC)')
ylabel('R0')
text(37,28,'A')

subplot('Position',[0.56 0.09 0.42 0.89])
plot(T,dR0.*R0,'k','LineWidth',2)
hold on
plot(T,(1./(2.*EFD)).*dEFD.*R0,'b','LineWidth',2)
plot(T,(1./(2.*pea)).*dPEA.*R0,'r','LineWidth',2)
plot(T,(1./(2.*MDR)).*dMDR.*R0,'g','LineWidth',2)
plot(T, -(3./(2*mu) + 1./(2*phi)).*dmu.*R0, 'm','LineWidth',2)
line([0 40],[0 0],'LineStyle','--','Color','k')
axis([13.5 30 -8 4])
legend('R0','EFD','Pea','MDR','\mu','Location','SouthWest')
xlabel('Temperature (\circC)')
ylabel('dR0/dT')
text(28.5,3.3,'B')



