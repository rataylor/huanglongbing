% New birth rate for the malaria model
% This file has the equations

% we add in a total vector pop with delay to the birth rate but for now we
% assume the delay is constant at 15 days  = 0.04 years

function f = citmaltestfun(t,y,Z,par)


%Seasonal forcing
T = 25*(1+0.3*sin(2*pi*t));

% All the parameters
a = par(1);
b = par(2);
c = par(3);
r = par(4);
r_1 = par(5);
mu = par(6);
EIP = par(7);
%lambda = par(8);
psus = par(9);
pinf = par(10);

pea = 0.47192 + 0.0109*T;
%MDR = 5.286e-05*365*T*(T - 10.02)*sqrt(34.17 - T) ;%*heaviside(T - 10.02)*heaviside(34.17 - T); %changed to Briere
%MDR = -0.043*(T - 11.635)*(T - 61.28)*heaviside(T - 11.635);
EFD = 0.0107*365*T*(T - 13)*sqrt(30.8 - T)*heaviside(T - 13)*heaviside(30.8 - T);
%mu = 9.215e-06*365*T*(T - 0)*sqrt(44.31 - T)*heaviside(T - 0)*heaviside(44.31 - T);
%mu = (-0.006036 + 0.001233*T)*365; %fit using table 4
%mu = (-0.014920 + 0.002173.*T).*365; %fit using fig 2
L = -0.14221*(T^2) + 4.31998*T + 31.25498; %fit using Table 4 plus freezing data
mu = (1/L)*365;
lambda =  EFD*pea/mu; %do i need to still multiple it by mdr?

% Model equations
S = y(1);
I = y(2);
R = y(3);
SV = y(4);
EV = y(5);
IV = y(6);
N = 101; %N = S + I; % we essentially have an exposed class of trees
V = SV + EV + IV;

K = 30000*N;
Kstar = K*(lambda/(lambda - mu));

ylag1 = Z(:,1); %first time lag for all y values
ylag2 = Z(:,2); %second time lag for all y values
ylag3 = Z(:,3); %third time lag - time to become adult


f = [-(a*b/N)*IV*S + r_1*I; ...
    (a*b/N)*ylag1(6)*ylag1(1) - r_1*I; ...
    r*S + r_1*I; ...
    lambda*(ylag3(4)+ylag3(5)+ylag3(6)) - (a*c/N)*SV*I - mu*SV; ...
    (a*c/N)*SV*I - (a*c*exp(-mu*EIP)/N)*ylag2(4)*ylag2(2) - mu*EV; ...
    (a*c*exp(-mu*EIP)/N)*ylag2(4)*ylag2(2) - mu*IV; ...
    psus*S + pinf*psus*I;];

% f = [-(a*b/N)*IV*S + r_1*I; ...
%     (a*b/N)*IV*S - r_1*I; ...
%     r*S + r_1*I; ...
%     lambda*V*(1-V/K) - (a*c/N)*SV*I - mu*SV; ...
%     (a*c/N)*SV*I - (a*c*exp(-mu*EIP)/N)*SV*I - mu*EV; ...
%     (a*c*exp(-mu*EIP)/N)*SV*I - mu*IV; ...
%     psus*S + pinf*psus*I;];
