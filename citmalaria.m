% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees

clear
format long
tic
% Parameter input
par = zeros(1,11);
par(1) = 0.05*365; % a
par(2) = 0.1; % b
par(3) = 0.3; % c
par(4) = 1/60; % r
par(5) = 1/10; % r_1
par(6) = 0.03631705*365; % mu
par(7) = 10/365; % EIP - number of days until psyllid becomes infectious
par(8) = 8000; % lambda
par(9) = 388; % psus - profit from a susceptible tree
par(10) = 0.4; % reduction in profit due to infection
rep = 35; % Cost of replacing a tree
r = 0.001; % discount factor

tau = 0.5; % host time delay
EIP = par(7); % psyllid time delay

% Initial conditions - need history of parameters since we have delay
% Initial infection starts exactly at t = 0 and not beforehand
S0 = 101;
I0 = 0;
R0 = 0;
SV0 = 500;
EV0 = 0;
IV0 = 0;
Prof0 = 0;

uhist = [S0,I0,R0,SV0,EV0,IV0,Prof0];
% Time solved for
tspan = [0,20];

% solver
options = ddeset('InitialY',[100,1,0,500,0,0,0],'reltol', 1e-8, 'abstol', 1e-6);
sol = dde23(@citmalariafun,[tau,EIP],uhist,tspan,options,par);

t = sol.x.';
y = sol.y.';

subplot('Position',[0.1 0.1 0.38 0.85]) %plotting tree solutions
plot(t,y(:,1),'LineWidth',2,'Color','b')
hold on
plot(t,y(:,2),'LineWidth',2,'Color','r')
hold on
plot(t,y(:,3),'LineWidth',2,'Color','m')
xlabel('Years')
ylabel('Trees')
axis([0 20 0 160])
legend('Susceptible','Infected','Removed','Location','NorthWest')
legend('boxoff')
% line([2,5],[170,170],'LineWidth',2,'Color','b')
% text(6,170,'Susceptible','Color','b')
% line([2,5],[163,163],'LineWidth',2,'Color','r')
% text(6,163,'Infected','Color','r')
% line([2,5],[156,156],'LineWidth',2,'Color','m')
% text(6,156,'Removed','Color','m')

subplot('Position',[0.58 0.1 0.38 0.85]) % plotting psyllids
plot(t,y(:,4),'LineWidth',2,'Color','b') 
hold on
plot(t,y(:,5),'LineWidth',2,'Color',[0 0.6 0.298])
hold on
plot(t,y(:,6),'LineWidth',2,'Color','r')
xlabel('Years')
ylabel('Psyllids')
axis([0 20 0 700])
legend('Susceptible','Exposed','Infected','Location','NorthEast')
legend('boxoff')
% line([9,12],[660,660],'LineWidth',2,'Color','b')
% text(13,660,'Susceptible','Color','b')
% line([9,12],[635,635],'LineWidth',2,'Color',[0 0.6 0.298])
% text(13,635,'Exposed','Color',[0 0.6 0.298])
% line([9,12],[610,610],'LineWidth',2,'Color','r')
% text(13,610,'Infected','Color','r')


%cost of replacement
crep = exp(-r.*t).*rep.*y(:,3);
income = exp(-r.*t).*y(:,7);
prof = income - crep;
A = [t,crep,income,prof];
%B = [t,crep,income,prof];
toc
%dlmwrite('costnoint.txt',A,'precision','%.6f');
%no disease
%dlmwrite('costnodis.txt',A,'precision','%.6f');


