% Plotting the profits of 4 antibiotic strategies alongside no intervention
% strategy

A = load('costnoint.txt');
B = load('anticosteps06kap03.txt');
C = load('anticosteps083kap03.txt');
D = load('anticosteps06kap09.txt');
E = load('anticosteps083kap09.txt');

plot(A(:,1),A(:,4),'k','LineWidth',2)
hold on
plot(B(:,1),B(:,5),'Color',rgb('Tan'),'LineWidth',2)
plot(C(:,1),C(:,5),'Color',rgb('FireBrick'),'LineWidth',2)
plot(D(:,1),D(:,5),'Color',rgb('MediumSpringGreen'),'LineWidth',2)
plot(E(:,1),E(:,5),'Color',rgb('MediumOrchid'),'LineWidth',2)
xlabel('Years')
ylabel('Total Profit ($)')
legend('No Intervention','LL','LH','HL','HH','Location','NorthWest')