% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees
% In this program we have a antibiotic intervention to test the
% effectiveness of this at stopping disease spread.
% This file has the equations

function f = citmalantifun(t,y,Z,par)


%Seasonal forcing
T = 25*(1+0.3*sin(2*pi*t));

% All the parameters
a = par(1);
b = par(2);
c = par(3);
r = par(4);
r_1 = par(5);
%mu = par(6);
EIP = par(7);
%lambda = par(8);

kappa = par(9);
eps = par(10);
r_2 = par(11); % or this could be defined in terms or r, r1 and eps.

%costs
psus = par(12);
pinf = par(13);

% pea = 0.47192 + 0.0109*T;
% MDR = -0.043*(T - 11.635)*(T - 61.28)*heaviside(T - 11.635);
% EFD = 0.0107*365*T*(T - 13)*sqrt(30.8 - T)*heaviside(T - 13)*heaviside(30.8 - T);
pea = 0.47192 + 0.0109*T;
MDR = 5.286e-05*365*T*(T - 10.02)*sqrt(34.17 - T)*heaviside(T - 10.02)*heaviside(34.17 - T); %changed to Briere
%MDR = -0.043*(T - 11.635)*(T - 61.28)*heaviside(T - 11.635);
EFD = 0.0107*365*T*(T - 13)*sqrt(30.8 - T)*heaviside(T - 13)*heaviside(30.8 - T);
%mu = 9.215e-06*365*T*(T - 0)*sqrt(44.31 - T)*heaviside(T - 0)*heaviside(44.31 - T);
%mu = (-0.006036 + 0.001233*T)*365; %fit using table 4
%mu = (-0.014920 + 0.002173.*T).*365; %fit using fig 2
L = (-0.14221*(T^2) + 4.31998*T + 31.25498)*heaviside(36.41 - T) + 0.01; %fit using Table 4 plus freezing data
mu = (1/L)*365;
lambda =  EFD*pea*MDR/mu;

% Model equations
S = y(1);
I = y(2);
J = y(3);
R = y(4);
SV = y(5);
EV = y(6);
IV = y(7);
N = 101;

ylag1 = Z(:,1); %first time lag for all y values
ylag2 = Z(:,2); %second time lag for all y values


f = [-(a*b/N)*IV*S + r_1*I + r_2*J; ...
    (a*b/N)*ylag1(7)*ylag1(1) - r_1*I - kappa*I; ...
    kappa*I - r_2*J; ...
    r*S + r_1*I + r_2*J; ...
    lambda - (a*c/N)*SV*I - (1-eps)*(a*c/N)*SV*J- mu*SV; ...
    (a*c/N)*SV*I + (1-eps)*(a*c/N)*SV*J - (a*c*exp(-mu*EIP)/N)*ylag2(5)*ylag2(2) - (1-eps)*(a*c*exp(-mu*EIP)/N)*ylag2(5)*ylag2(3)- mu*EV; ...
    (a*c*exp(-mu*EIP)/N)*ylag2(5)*ylag2(2) + (1-eps)*(a*c*exp(-mu*EIP)/N)*ylag2(5)*ylag2(3) - mu*IV; ...
    kappa*I; ...
    psus*S + pinf*psus*I + (pinf + (1-pinf)*eps)*psus*J;];

