%to plot the different curves of total profit over time on one plot

A = load('costnodis.txt');
B = load('costnoint.txt');
C = load('insectcostm30r06Spray2.txt');
%D = load('anticosteps083kap09.txt');

plot(A(:,1),A(:,4),'Color',rgb('DarkSeaGreen'),'LineWidth',2)
hold on
plot(B(:,1),B(:,4),'k','LineWidth',2)
hold on
plot(C(:,1),C(:,5),'Color',[0 0.502 1],'LineWidth',2)
%hold on
%plot(D(:,1),D(:,5),'Color',rgb('MediumOrchid'),'LineWidth',2)
xlabel('Years')
ylabel('Total Profit ($)')
legend('No Disease','No Intervention','Insecticide','Location','NorthWest')
