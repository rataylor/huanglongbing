% Same as citmalaria, but a loop is used to test different roguing
% strategies. We measure peak number of infected trees and number of
% removed trees

% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees

clear
format long
tic
% Parameter input
par = zeros(1,11);
par(1) = 0.05*365; % a
par(2) = 0.1; % b
par(3) = 0.3; % c
par(4) = 1/60; % r
%par(5) = 1/10; % r_1
par(6) = 0.03631705*365; % mu
par(7) = 10/365; % EIP - number of days until psyllid becomes infectious
par(8) = 8000; % lambda
par(9) = 388; % psus - profit from a susceptible tree
par(10) = 0.4; % reduction in profit due to infection
rep = 50; % Cost of replacing a tree
r = 0.001; % discount factor

tau = 0.5; % host time delay
EIP = par(7); % psyllid time delay

% Initial conditions - need history of parameters since we have delay
% Initial infection starts exactly at t = 0 and not beforehand
S0 = 101;
I0 = 0;
R0 = 0;
SV0 = 500;
EV0 = 0;
IV0 = 0;
Prof0 = 0;

uhist = [S0,I0,R0,SV0,EV0,IV0,Prof0];
% Time solved for
tspan = [0,20];

r_1 = [1/15,1/12,1/10,1/8,1/6,1/4];
A = zeros(6,3);
for i = 1:6
    % solver
    par(5) = r_1(i);
    options = ddeset('InitialY',[100,1,0,500,0,0,0],'reltol', 1e-8, 'abstol', 1e-6);
    sol = dde23(@citmalariafun,[tau,EIP],uhist,tspan,options,par);

    t = sol.x.';
    y = sol.y.';
    A(i,:) = [r_1(i),max(y(:,2)),y(end,3)];
end

dlmwrite('VaryRogue.txt',A,'precision','%.6f');