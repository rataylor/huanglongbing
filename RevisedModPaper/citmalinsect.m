% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees
% insecticide spraying

clear
format long
tic
% Parameter input
par = zeros(1,13);
par(1) = 0.05*365; % a
par(2) = 0.025; % 0.1; b
par(3) = 0.02; % 0.3; c
par(4) = 1/60; % r
par(5) = 1/7; % r_1
par(6) = 10/365; % EIP - number of days until psyllid becomes infectious
%parameters related to insecticide spray
par(7) =0; %3*pi/2; % phase for time of year of spray, 0, pi/2, pi, 3pi/2
par(8) = 30; % m, number of days spraying
par(9) = 0.6; % rho, effectiveness of spray
%cost parameters
par(10) = 388; %profit of susceptible tree
par(11) = 0.4; %profit reduction for infection
rep = 35; %cost of replacing a tree
inscost = 15; %cost of 1 day of insecticide
discountr = 0.001; %discount factor

tau = 0.5; % host time delay
par(12) = tau;

par(13) = 1/3; % gam - the length of time asymptomatic - reduces r1 so that trees infectious are rogued sooner

% Initial conditions - need history of parameters since we have delay
S0 = 1001;
A0 = 0;
I0 = 0;
R0 = 0;
SV0 = 500;
EV10 = 0;
EV20 = 0;
EV30 = 0;
IV0 = 0;
Prof0 = 0;

uhist = [S0,A0,I0,R0,SV0,EV10,EV20,EV30,IV0,Prof0];


% Time solved for
tspan = [0,20];

% solver
options = ddeset('InitialY',[1000,0,1,0,500,0,0,0,0,0],'reltol', 1e-8, 'abstol', 1e-6);
sol = dde23(@citmalinsSpray2fun,tau,uhist,tspan,options,par);

t = sol.x.';
y = sol.y.';

subplot('Position',[0.1 0.1 0.38 0.85]) %plotting tree solutions
plot(t,y(:,1),'LineWidth',2,'Color','b')
hold on
plot(t,y(:,2),'LineWidth',2,'Color','g')
hold on
plot(t,y(:,3),'LineWidth',2,'Color','r')
hold on
plot(t,y(:,4),'LineWidth',2,'Color','m')
xlabel('Years')
ylabel('Trees')
axis([0 10 0 1100])
legend('Susceptible','Asymptomatic','Infected','Removed','Location','NorthWest')
legend('boxoff')
% line([2,5],[170,170],'LineWidth',2,'Color','b')
% text(6,170,'Susceptible','Color','b')
% line([2,5],[163,163],'LineWidth',2,'Color','r')
% text(6,163,'Infected','Color','r')
% line([2,5],[156,156],'LineWidth',2,'Color','m')
% text(6,156,'Removed','Color','m')

subplot('Position',[0.58 0.1 0.38 0.85]) % plotting psyllids
plot(t,y(:,5),'LineWidth',2,'Color','b') 
hold on
plot(t,y(:,6)+y(:,7)+y(:,8),'LineWidth',2,'Color',[0 0.6 0.298])
hold on
plot(t,y(:,9),'LineWidth',2,'Color','r')
xlabel('Years')
ylabel('Psyllids')
axis([0 10 0 5000000])
legend('Susceptible','Exposed','Infected','Location','NorthEast')
legend('boxoff')
% line([9,12],[660,660],'LineWidth',2,'Color','b')
% text(13,660,'Susceptible','Color','b')
% line([9,12],[635,635],'LineWidth',2,'Color',[0 0.6 0.298])
% text(13,635,'Exposed','Color',[0 0.6 0.298])
% line([9,12],[610,610],'LineWidth',2,'Color','r')
% text(13,610,'Infected','Color','r')

max(y(end/2:end,1))
y(end,3)
max(y(:,9))

%cost of spraying 
crep = exp(-discountr.*t).*rep.*y(:,4);
cins = exp(-discountr.*t).*t.*2*par(8).*inscost;
inscost2 = 15.075*par(9)/(1.203-par(9));
cins2 = exp(-discountr.*t).*t.*par(8)*2.*inscost2;
income = exp(-discountr.*t).*y(:,10);
prof = income - crep - cins;
prof2 = income - crep - cins2; 
A = [t,crep,cins,income,prof,prof2];
toc
%dlmwrite('insectcostm30r06Spray2ASYF.txt',A,'precision','%.6f');