%testing new seasonal flush patterns.

N = 101; %total number of trees
t = 0:0.001:1; %time
F = N.*(0 + 8.*heaviside(0.5*(1/12)-t) + 6.*heaviside(t - 1.5*(1/12)).*heaviside(3.5*(1/12) - t) + 3.*heaviside(t-4.5*(1/12)).*heaviside(5.5*(1/12)-t) + 8.*heaviside(t-9.5*(1/12)));

plot(t,F)