% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees
% ALTERED: This is new model for the resubmission of PeerJ paper
% This file has the equations

function f = citmalariafun(t,y,Z,par)


%Seasonal forcing
T = 25*(1+0.3*sin(2*pi*t));

% All the parameters
a = par(1);
b = par(2);
c = par(3);
r = par(4);
r_1 = par(5);
%mu = par(6);
EIP = par(7); 
phi = 1/EIP;
%lambda = par(8);
psus = par(9);
pinf = par(10);
tau = par(11);
gam = par(12);

pea = 0.47192 + 0.0109*T;
MDR = 5.286e-05*365*T*(T - 10.02)*sqrt(34.17 - T) ;%*heaviside(T - 10.02)*heaviside(34.17 - T); %changed to Briere
%MDR = -0.043*(T - 11.635)*(T - 61.28)*heaviside(T - 11.635);
EFD = 0.0107*365*T*(T - 13)*sqrt(30.8 - T)*heaviside(T - 13)*heaviside(30.8 - T);
%mu = 9.215e-06*365*T*(T - 0)*sqrt(44.31 - T)*heaviside(T - 0)*heaviside(44.31 - T);
%mu = (-0.006036 + 0.001233*T)*365; %fit using table 4
%mu = (-0.014920 + 0.002173.*T).*365; %fit using fig 2
L = -0.14221*(T^2) + 4.31998*T + 31.25498; %fit using Table 4 plus freezing data
mu = (1/L)*365;
lambda =  EFD*pea*MDR/mu;

% Model equations
S = y(1);
A = y(2);
I = y(3);
R = y(4);
SV = y(5);
EV1 = y(6);
EV2 = y(7);
EV3 = y(8);
IV = y(9);
N = 1001;

%t2 = floor(t);
%tnew = t - t2;
%F = N*(1 + 8.*heaviside(2*(1/12)-tnew) + 6*heaviside(tnew - 3*(1/12))*heaviside(5*(1/12) - tnew) + 3*heaviside(tnew-7*(1/12))*heaviside(8*(1/12)-tnew) + 8*heaviside(tnew-11*(1/12)));
%F = N.*(0 + 8.*heaviside(0.5*(1/12)-tnew) + 6.*heaviside(tnew - 1.5*(1/12)).*heaviside(3.5*(1/12) - tnew) + 3.*heaviside(tnew-4.5*(1/12)).*heaviside(5.5*(1/12)-tnew) + 8.*heaviside(tnew-9.5*(1/12)));
% second term has 0 during parts of year and year starts mid-April
%sinusoidal term that has flush in springtime and autumn.
F = N*3*(1+sin(4*pi*t + pi/2));

ylag1 = Z(:,1); %first time lag for all y values
%ylag2 = Z(:,2); %second time lag for all y values


f = [-(a*b/N)*IV*S - r*S + r*(N-I) + r_1*I; ...
    (a*b/N)*ylag1(9)*ylag1(1)*exp(-r*tau) - gam*A - r*A; ...
    gam*A - r_1*I; ...
    r*(N-I) + r_1*I; ...
    lambda*F - (a*c/N)*SV*(A+I) - mu*SV; ...
    (a*c/N)*SV*(A+I) - 3*phi*EV1 - mu*EV1; ...
    3*phi*EV1 - 3*phi*EV2 - mu*EV2; ...
    3*phi*EV2 - 3*phi*EV3 - mu*EV3; ...
    3*phi*EV3 - mu*IV; ...
    psus*(N-I) + pinf*psus*I;];
