%to plot the different curves of total profit over time on one plot
clear
A = load('costnodisASYF.txt');
B = load('costnointASYF.txt');
C = load('insectcostm30r06Spray2ASYF.txt');
%D = load('anticosteps083kap09.txt');

plot(A(:,1),A(:,4),'Color',[0.35 0.7 0.9],'LineWidth',2,'LineStyle','-.')
hold on
plot(B(:,1),B(:,4),'k','LineWidth',2)
hold on
plot(C(:,1),C(:,6),'Color',[0.8 0.6 0.7],'LineWidth',2,'LineStyle','--')
%hold on
%plot(D(:,1),D(:,5),'Color',rgb('MediumOrchid'),'LineWidth',2)
xlabel('Years')
ylabel('Total Profit ($)')
legend('No Disease','No Intervention','Insecticide','Location','NorthWest')
