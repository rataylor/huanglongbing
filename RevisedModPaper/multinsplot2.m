% Creates a colour plot based on the output from citmalinsmult.m
%This file takes two different text files and plots two figures together
clear
%%%%%%%% Note that for SW spraying, I changed the roguing cost to $35 and
% the insecticide to be between 15 - 70. But for the SA one (original) it
% is still at 50 and 30-90. Thus it needs changed first.

A = load('FlushInsSAsprayFinal_ALL.txt');
B = load('FlushInsSWsprayFinal.txt');

%note that A[:,1] = m Number of Days spraying
% A[:,2] = rho Effectiveness of spray
% A[:,3] = peak number of asymptomatic
% A(:,4] = peak number of infectious
% A[:,5] = max number of susceptibles in second half
% A[:,6] = max number of infectious psyllids
% A[:,7] = total number of removed trees
% A[:,8] = profit without considering increased costs
% A[:,9] = profit with increased costs for more efficient insecticide


%Making changes for costs for A only
m = 5:30;
rho = 0.6:0.01:0.99;
n1 = size(m,2);
n2 = size(rho,2);

%what if we reduce the cost of removing trees to $35? Already done for this
%model
% r = 0.001;
% rep = 50;
% rep2 = 35;
% crep = exp(-r.*20).*rep.*A(:,5);
% crep2 = exp(-r.*20).*rep2.*A(:,5);
% A(:,7) = A(:,7) + crep - crep2;

%Change inscosts to vary between 15 and 70 rather than 20 to 90. Also done
%for this model
% oldinscost = 23.57.*rho./(1.25-rho);
% newinscost = 15.075.*rho./(1.203-rho);
% for i = 1:n1
%     oldcost = exp(-r.*20).*20.*m(i).*oldinscost;
%     newcost = exp(-r.*20).*20.*m(i)*2.*newinscost;
%     A((i-1)*n2+1:i*n2,7) = A((i-1)*n2+1:i*n2,7) + oldcost.' - newcost.';
% end


%in order to do a colour plot need to transform A and B into matrix format for
%just 1 response
for i = 1:n1
    C1(1:n2,i) = A((i-1)*n2+1:i*n2,6); %looking at col 4 = peak infectiousness
    D1(1:n2,i) = B((i-1)*n2+1:i*n2,6); 
end

%Turn right way up for colour plot
C = flipud(C1);
D = flipud(D1);

        
%%%%% Plotting commands 4x4 plot with ony colour bar

colormap(parula)

%ax1 = axes('Position',[0 0 1 1],'Visible','off'); %invisible axes for writing text outside plots
%ax2 = axes('position',[0.09 0.55 0.38 0.395]);
%axes(ax2)

% imagesc(m,rho,C)
% %caxis([650.6920  658.3850])
% h1 = colorbar;
% xlabel('Number of Days Spraying')
% set(gca,'XTick',[10,20,30])
% set(gca,'XTickLabel',{'20','40','60'})
% ylabel('Effectiveness of Spray (%)') %note axis wrong way round here
% ylabel(h1,'Peak Number of Infected Psyllids')
% set(gca,'YTick',[0.6 0.69 0.79 0.89 0.99])
% set(gca,'YTicklabel',{'0.99','0.9','0.8','0.7','0.6'})


%ax3 = axes('position',[0.09 0.07 0.38 0.395]);
subplot('position',[0.08 0.08 0.38 0.88]) %changing to two plots for presentation
imagesc(m,rho,D)
%caxis([650.6920  658.3850]) %scale of SA plot
h2 = colorbar;
xlabel('Number of Days Spraying')
set(gca,'XTick',[10,20,30])
set(gca,'XTickLabel',{'20','40','60'})
ylabel('Effectiveness of Spray (%)') %note axis wrong way round here
ylabel(h2,'Peak Number of Infected Psyllids')
set(gca,'YTick',[0.6 0.69 0.79 0.89 0.99])
set(gca,'YTicklabel',{'0.99','0.9','0.8','0.7','0.6'})

% x = linspace(0,40000,size(A(:,2),1)); %colour bar for second type of plot
% 
% ax4 = axes('position',[0.61 0.55 0.38 0.395]);
% for i = 1:size(A(:,2),1) %trying to plot the different rows as different colours
%     col = A(i,1)/max(A(:,1)); %move from white to black depending on number of days spraying
%     plot(A(i,2),A(i,9),'*','Color',[0 0.502 1]*col) %plotting effectiveness against profit
%     hold on
%     plot(0.97,5450000+x(i),'.','Color',[0 0.502 1]*col) %plotting colour bar
%     hold on
% end
% axis([0.6 1 5250000 5500000])
% xlabel('Effectiveness of Spray (%)')
% ylabel('Profit')
% text(0.93,5460000,'10')
% text(0.93,5485000,'60')

x1 = linspace(0,15000,size(B(:,2),1)); %colour bar for second type of plot

%ax5 = axes('position',[0.61 0.07 0.38 0.395]);
subplot('position',[0.58 0.08 0.38 0.88])
for i = 1:size(B(:,2),1) %trying to plot the different rows as different colours
    col = B(i,1)/max(B(:,1)); %move from white to black depending on number of days spraying
    plot(B(i,2),B(i,9),'*','Color',[0 0.502 1]*col) %plotting effectiveness
    hold on
    plot(0.65,5230000+x1(i),'.','Color',[0 0.502 1]*col) %plotting colour bar
    hold on
end
axis([0.6 1 5220000 5300000])
xlabel('Effectiveness of Spray (%)')
ylabel('Profit')
text(0.66,5231000,'10')
text(0.66,5244000,'60')

% %figure labels
% axes(ax1) %set current axes
% text(0.03,0.98,'{\bf A}')
% text(0.52,0.98,'{\bf B}')
% text(0.03,0.5, '{\bf C}')
% text(0.52,0.5,'{\bf D}')
