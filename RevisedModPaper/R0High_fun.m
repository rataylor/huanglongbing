function R0High = R0High_fun(v)

% calculating r0 at the temperature that makes r0 its maximum.

%T = 23.43;
% new temp for new R0 model with asymptomatic etc.
T = 24.25;

N=1001;

pea = 0.47192 + 0.0109.*T;
%MDR = -0.00012.*365.*(T - 11.635).*(T - 61.28).*heaviside(T - 11.635);
MDR = 5.286e-05.*365.*T.*(T - 10.02).*sqrt(34.17 - T).*heaviside(T - 10.02).*heaviside(34.17 - T); %MDR as briere
EFD = 0.0107.*365.*T.*(T - 13).*sqrt(30.8 - T).*heaviside(T - 13).*heaviside(30.8 - T);
%mu = (-0.006036 + 0.001233.*T).*365; using longevity table
%mu = (-0.014920 + 0.002173.*T).*365; %Different fit using fig 2 instead
L = -0.14221.*(T.^2) + 4.31998.*T + 31.25498; %longevity with new cold data
mu = (1./L)*365;
F = 6.*N.*(1-((10/3).*(T/25 - 1)).^2);

a = v(1);
b = v(2);
c = v(3);
r1 = v(4);
phi = v(5);
gam = v(6);
r = v(7);
tau = v(8);

R0High = ((EFD.*pea.*MDR.*(a.^2).*b.*c.*F./(N.*(mu.^3))).*(1/(gam+r) + gam/((gam+r)*r1)).*exp(-r*tau).*(3*phi./(3*phi + mu)).^3).^(0.5);