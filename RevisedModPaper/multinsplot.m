% Creates a colour plot based on the output from citmalinsmult.m 
% That data was saved in a file called MalInsmult.txt
% It continues 6 columns, namely, the value of m, number of days spraying,
% rho, the effectiveness of spray, the peak number of infectives over the
% 20 years, the total number of trees removed over the 20 years, the profit
% based off of set costs, and the profit where the cost of insecticide
% treatment increases with its effectiveness.
clear

%A = load('MalinsmultSpray2.txt');
A = load('FlushInsSAsprayFinal_ALL.txt');
%B = load('MalInsmult.txt');

%note that A[:,1] = m Number of Days spraying
% A[:,2] = rho Effectiveness of spray
% A[:,3] = peak number of asymptomatic
% A(:,4] = peak number of infectious
% A[:,5] = max number of susceptibles in second half
% A[:,6] = max number of infectious psyllids
% A[:,7] = total number of removed trees
% A[:,8] = profit without considering increased costs
% A[:,9] = profit with increased costs for more efficient insecticide

plot(A(:,1),A(:,3),'*')
xlabel('Days Spraying')
ylabel('Peak Number of Infectives')
figure
plot(A(:,2),A(:,3),'*')
xlabel('Effectiveness of Spray')
ylabel('Peak Number of Infectives')
figure
plot(A(:,1),A(:,4),'*')
figure
plot(A(:,2),A(:,4),'*')
figure
plot(A(:,2),A(:,5),'*')
figure
plot(A(:,1),A(:,5),'*')
figure
plot(A(:,2),A(:,6),'*')
figure
plot(A(:,1),A(:,6),'*')
figure
plot(A(:,2),A(:,7),'*')
figure
plot(A(:,1),A(:,7),'*')

m = 5:30;
rho = 0.6:0.01:0.99;
n1 = size(m,2);
n2 = size(rho,2);

%what if we reduce the cost of removing trees to $35?
% r = 0.001;
% rep = 50;
% rep2 = 35;
% crep = exp(-r.*20).*rep.*A(:,5);
% crep2 = exp(-r.*20).*rep2.*A(:,5);
% A(:,7) = A(:,7) + crep - crep2;

%Change inscosts to vary between 15 and 70 rather than 20 to 90
% oldinscost = 23.57.*rho./(1.25-rho);
% newinscost = 15.075.*rho./(1.203-rho);
% for i = 1:n1
%     oldcost = exp(-r.*20).*20.*m(i).*oldinscost;
%     newcost = exp(-r.*20).*20.*m(i)*2.*newinscost;
%     A((i-1)*n2+1:i*n2,7) = A((i-1)*n2+1:i*n2,7) + oldcost.' - newcost.';
% end


%in order to do a colour plot need to transform A into matrix format for
%just 1 response
for i = 1:n1
    B(1:n2,i) = A((i-1)*n2+1:i*n2,6);
end

C = flipud(B);
%subplot('position',[0.082 0.1 0.4 0.85])
imagesc(m,rho,C)
h1 = colorbar;
xlabel('Number of Days Spraying')
set(gca,'XTickLabel',{'10','20','30','40','50','60'})
ylabel('Effectiveness of Spray (%)') %note axis wrong way round here
ylabel(h1,'Peak Number of Infected Trees')
set(gca,'YTick',[0.6 0.69 0.79 0.89 0.99])
set(gca,'YTicklabel',{'0.99','0.9','0.8','0.7','0.6'})
%annotation('textbox',[0.35 0.88 0.03 0.05],'String','A','BackgroundColor','white')

x = linspace(0,17000,size(A(:,2),1)); %colour bar
subplot('position',[0.6 0.1 0.38 0.85])
for i = 1:size(A(:,2),1) %trying to plot the different rows as different colours
    col = A(i,1)/max(A(:,1)); %move from white to black depending on number of days spraying
    plot(A(i,2),A(i,9),'*','Color',[0 0.502 1]*col) %plotting effectiveness
    hold on
    %plot(0.97,518000+x(i),'.','Color',[0 0.502 1]*col) %plotting colour bar
    hold on
end
%axis([0.6 1 330000 410000])
xlabel('Effectiveness of Spray (%)')
ylabel('Profit')
text(0.94,519000,'10')
text(0.94,535000,'60')
annotation('textbox',[0.63 0.88 0.03 0.05],'String','B','BackgroundColor','white')

