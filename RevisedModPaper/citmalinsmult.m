% In this program we will run the solver multiple times, in a grid fashion,
% in order to produce a colour plot of some measure of the disease (peak
% number of infectives, total number removed) against both effectiveness of
% spray and number of days spraying. We can also do a colour plot of cost
% of each strategy beside it.

clear
format long

% Parameter input
par = zeros(1,13);
par(1) = 0.05*365; % a
par(2) = 0.025; %0.1; % b
par(3) = 0.02; %0.3; % c
par(4) = 1/60; % r
par(5) = 1/7; % r_1
par(6) = 10/365; % EIP - number of days until psyllid becomes infectious
%parameters related to insecticide spray
par(7) = 0; %3*pi/2; % phase for time of year of spray, 0, pi/2, pi, 3pi/2
%par(8) = 20; % m, number of days spraying - we vary this
%par(9) = 0.95; % rho, effectiveness of spray - we vary this
%cost parameters
par(10) = 388; %profit of susceptible tree
par(11) = 0.4; %profit reduction for infection
rep = 35; %cost of replacing a tree 
inscost1 = 30; %cost of 1 day of insecticide
discountr = 0.001; % discount factor

tau = 0.5; % host time delay
par(12) = tau;

par(13) = 1/3; % gam - the length of time asymptomatic - reduces r1 so that trees infectious are rogued sooner
% EIP = par(7); % psyllid time delay

%EIP = 10/365; % psyllid time delay

m = 5:30;
rho = 0.6:0.01:0.99;


% Initial conditions - need history of parameters since we have delay
S0 = 1001;
A0 = 0;
I0 = 0;
R0 = 0;
SV0 = 500;
EV10 = 0;
EV20 = 0;
EV30 = 0;
IV0 = 0;
Prof0 = 0;

uhist = [S0,A0,I0,R0,SV0,EV10,EV20,EV30,IV0,Prof0];

% Time solved for
tspan = [0,20];


for k = 9:size(m,2)
    if k == 9 
        rho = 0.9:0.01:0.99;
    else
        rho = 0.6:0.01:0.99;
    end
    for j = 1:size(rho,2)
% %for k = 1:26
% k=5;
%     for j = 3:40
        
        fid = fopen('FlushInsSAsprayFinal2.txt', 'a+');
        
        par(8) = m(k);
        par(9) = rho(j);

        % solver
        options = ddeset('InitialY',[1000,0,1,0,500,0,0,0,0,0],'reltol', 1e-8, 'abstol', 1e-6);
        sol = dde23(@citmalinsSpray2fun,tau,uhist,tspan,options,par);

        t = sol.x.';
        y = sol.y.';
        
        peakAsy = max(y(:,2)); %peak in asymptomatics
        %apeak = find(y(:,2) > peakAsy*0.95, 1);
        %tMaxA = t(apeak);
        peakInf = max(y(:,3)); %peak in infected trees
        %ypeak = find(y(:,3) > peakInf*0.95,1);
        %tMax = t(ypeak);
        
        
        peakIP = max(y(:,9)); %max no of infectious psyllids
        maxSupend = max(y(end/2:end,1)); %max no of susceptible in second half of simulation
        
        totalRem = y(end,4);
        crep = exp(-discountr.*t).*rep.*y(:,4);
        %inscost2 = 23.57*par(9)/(1.25-par(9)); %varying cost dependent on spray effectiveness
        % change costs if run again - 
        inscost2 = 15.075*par(9)/(1.203-par(9));
        cins1 = exp(-discountr.*t).*t.*par(8)*2.*inscost1;
        cins2 = exp(-discountr.*t).*t.*par(8)*2.*inscost2;  %changed *2 - due to days spraying is twice this amount
        income = exp(-discountr.*t).*y(:,10);
        prof1 = income(end) - crep(end) - cins1(end); % two different profits depending on constant 
        prof2 = income(end) - crep(end) - cins2(end); % cost or varying cost
        
        
        fprintf(fid, '%g %g %g %g %g %g %g %g %g\n', m(k), rho(j), peakAsy, peakInf, maxSupend, peakIP, totalRem, prof1, prof2);
        fclose(fid);
        
    end
end

