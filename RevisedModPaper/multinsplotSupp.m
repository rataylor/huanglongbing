% Creates a colour plot based on the output from citmalinsmult.m
%This file takes two different text files and plots two figures together
clear

%%% This is for the appendix with just colour plots and no profits plots

A = load('FlushInsSAsprayFinal_ALL.txt');
B = load('FlushInsSWsprayFinal.txt');

%note that A[:,1] = m Number of Days spraying
% A[:,2] = rho Effectiveness of spray
% A[:,3] = peak number of asymptomatic
% A(:,4] = peak number of infectious
% A[:,5] = max number of susceptibles in second half
% A[:,6] = max number of infectious psyllids
% A[:,7] = total number of removed trees
% A[:,8] = profit without considering increased costs
% A[:,9] = profit with increased costs for more efficient insecticide


%Making changes for costs for A only
m = 5:30;
rho = 0.6:0.01:0.99;
n1 = size(m,2);
n2 = size(rho,2);

%what if we reduce the cost of removing trees to $35? Already done for this
%model
% r = 0.001;
% rep = 50;
% rep2 = 35;
% crep = exp(-r.*20).*rep.*A(:,5);
% crep2 = exp(-r.*20).*rep2.*A(:,5);
% A(:,7) = A(:,7) + crep - crep2;

%Change inscosts to vary between 15 and 70 rather than 20 to 90. Also done
%for this model
% oldinscost = 23.57.*rho./(1.25-rho);
% newinscost = 15.075.*rho./(1.203-rho);
% for i = 1:n1
%     oldcost = exp(-r.*20).*20.*m(i).*oldinscost;
%     newcost = exp(-r.*20).*20.*m(i)*2.*newinscost;
%     A((i-1)*n2+1:i*n2,7) = A((i-1)*n2+1:i*n2,7) + oldcost.' - newcost.';
% end


%in order to do a colour plot need to transform A and B into matrix format for
%just 1 response
for i = 1:n1
    C1(1:n2,i) = A((i-1)*n2+1:i*n2,4); %looking at col 4 = peak infectiousness
    D1(1:n2,i) = B((i-1)*n2+1:i*n2,4); 
    E1(1:n2,i) = A((i-1)*n2+1:i*n2,5);
    F1(1:n2,i) = B((i-1)*n2+1:i*n2,5);
end

%Turn right way up for colour plot
C = flipud(C1);
D = flipud(D1);
E = flipud(E1);
F = flipud(F1);

        
%%%%% Plotting commands 4x4 plot with ony colour bar

colormap(parula)

ax1 = axes('Position',[0 0 1 1],'Visible','off'); %invisible axes for writing text outside plots
ax2 = axes('position',[0.09 0.55 0.36 0.39]);
axes(ax2)
%subplot('position',[0.08 0.55 0.38 0.4])
imagesc(m,rho,C)
caxis([650.6920  658.3850])
h1 = colorbar;
%xlabel('Number of Days Spraying')
set(gca,'XTick',[10,20,30])
set(gca,'XTickLabel',{'20','40','60'})
ylabel('Effectiveness of Spray (%)') %note axis wrong way round here
ylabel(h1,'Peak Number of Infected Trees')
set(gca,'YTick',[0.6 0.69 0.79 0.89 0.99])
set(gca,'YTicklabel',{'0.99','0.9','0.8','0.7','0.6'})


ax3 = axes('position',[0.09 0.07 0.36 0.39]);
imagesc(m,rho,D)
caxis([650.6920  658.3850]) %scale of SA plot
h2 = colorbar;
xlabel('Number of Days Spraying')
set(gca,'XTick',[10,20,30])
set(gca,'XTickLabel',{'20','40','60'})
ylabel('Effectiveness of Spray (%)') %note axis wrong way round here
ylabel(h2,'Peak Number of Infected Trees')
set(gca,'YTick',[0.6 0.69 0.79 0.89 0.99])
set(gca,'YTicklabel',{'0.99','0.9','0.8','0.7','0.6'})


ax4 = axes('position',[0.59 0.55 0.36 0.39]);
imagesc(m,rho,E)
caxis([24.7977  43.0725]) %scale of SA plot
h2 = colorbar;
%xlabel('Number of Days Spraying')
set(gca,'XTick',[10,20,30])
set(gca,'XTickLabel',{'20','40','60'})
ylabel('Effectiveness of Spray (%)') %note axis wrong way round here
ylabel(h2,'Peak Recovery of Susceptible Trees')
set(gca,'YTick',[0.6 0.69 0.79 0.89 0.99])
set(gca,'YTicklabel',{'0.99','0.9','0.8','0.7','0.6'})

ax5 = axes('position',[0.59 0.07 0.36 0.39]);
imagesc(m,rho,F)
caxis([24.7977  43.0725])
h2 = colorbar;
xlabel('Number of Days Spraying')
set(gca,'XTick',[10,20,30])
set(gca,'XTickLabel',{'20','40','60'})
ylabel('Effectiveness of Spray (%)') %note axis wrong way round here
ylabel(h2,'Peak Recovery of Susceptible Trees')
set(gca,'YTick',[0.6 0.69 0.79 0.89 0.99])
set(gca,'YTicklabel',{'0.99','0.9','0.8','0.7','0.6'})

%figure labels
axes(ax1) %set current axes
text(0.03,0.98,'{\bf A}')
text(0.52,0.98,'{\bf B}')
text(0.03,0.5, '{\bf C}')
text(0.52,0.5,'{\bf D}')
