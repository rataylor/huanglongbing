% Plotting lambda and r0 based on temperature or over time
clear
% Temperature dependency
T = 10:0.01:40;

pea = 0.47192 + 0.0109.*T;
%MDR = -0.00012.*365.*(T - 11.635).*(T - 61.28).*heaviside(T - 11.635);
MDR = 5.286e-05.*365.*T.*(T - 10.02).*sqrt(34.17 - T).*heaviside(T - 10.02).*heaviside(34.17 - T); %MDR as briere
EFD = 0.0107.*365.*T.*(T - 13).*sqrt(30.8 - T).*heaviside(T - 13).*heaviside(30.8 - T);
%mu = (-0.006036 + 0.001233.*T).*365; using longevity table
%mu = (-0.014920 + 0.002173.*T).*365; %Different fit using fig 2 instead
L = -0.14221.*(T.^2) + 4.31998.*T + 31.25498; %longevity with new cold data
mu = (1./L)*365;
lambda = EFD.*pea.*MDR./mu;

%ro calc for temperature

a = 0.05*365; %0.05*365;
b = 0.025; %0.1;
c = 0.02; %0.3;
N = 1001;
r1 = 1/7;
r = 1/60;
phi = 36.5;
gam = 1/3;
tau = 0.5;
F = 6.*N.*(1-((10/3).*(T/25 - 1)).^2);
F = F.*heaviside(F);
%F=1;

r0 = ((lambda.*F.*(a^2)*b*c./(N.*(mu.^2))).*(1/(gam+r) + gam/((gam+r)*r1)).*exp(-r*tau).*(3*phi./(3*phi + mu)).^3).^(0.5);
% figure
% plot(T,r0,'k','LineWidth',2,'Color','b')
% xlabel('Temperature')
% ylabel('R_0')

%time dependency
t = 0:0.01:2;
T2 = 25.*(1+0.3.*sin(2*pi.*t));
F2 = N*3.*(1+sin(4*pi.*t + pi/2));
%F2 = 1;

peaT = 0.47192 + 0.0109.*T2;
%MDR = -0.00012.*365.*(T - 11.635).*(T - 61.28).*heaviside(T - 11.635);
MDRT = 5.286e-05.*365.*T2.*(T2 - 10.02).*sqrt(34.17 - T2).*heaviside(T2 - 10.02).*heaviside(34.17 - T2); %MDR as briere
EFDT = 0.0107.*365.*T2.*(T2 - 13).*sqrt(30.8 - T2).*heaviside(T2 - 13).*heaviside(30.8 - T2);
%mu = (-0.006036 + 0.001233.*T).*365; using longevity table
%mu = (-0.014920 + 0.002173.*T).*365; %Different fit using fig 2 instead
LT = -0.14221.*(T2.^2) + 4.31998.*T2 + 31.25498; %longevity with new cold data
muT = (1./LT)*365;
lambdaT = EFDT.*peaT.*MDRT./muT;


r0T = ((lambdaT.*F2.*(a^2).*b.*c./(N.*(muT.^2))).*(1/(gam+r) + gam/((gam+r)*r1)).*exp(-r*tau).*(3.*phi./(3.*phi + muT)).^3).^(0.5);


subplot('position',[0.1 0.1 0.4 0.85])
plot(T,r0,'b','LineWidth', 2)
xlabel('Temperature (\circ C)')
ylabel('R_0')
subplot('position',[0.55 0.1 0.4 0.85])
plot(t,r0T,'b','LineWidth', 2)
xlabel('Time (years)')
