% Sensitivity analysis for the constant parameters
% Change each one One At A Time by a certain percentage and plot how that
% affects R0

% This is for the malaria based model 

clear

T = 10:0.001:40;

a = 0.05*365;
b = 0.1;
c = 0.3;
r1 = 1/10;
phi = 36.5; % the same as PDR = 1/EIP implies it takes 10 days for psyllid to become infectious
N = 101;

pea = 0.47192 + 0.0109.*T;
%MDR = -0.00012.*365.*(T - 11.635).*(T - 61.28).*heaviside(T - 11.635);
MDR = 5.286e-05.*365.*T.*(T - 10.02).*sqrt(34.17 - T).*heaviside(T - 10.02).*heaviside(34.17 - T); %MDR as briere
EFD = 0.0107.*365.*T.*(T - 13).*sqrt(30.8 - T).*heaviside(T - 13).*heaviside(30.8 - T);
%mu = (-0.006036 + 0.001233.*T).*365; using longevity table
%mu = (-0.014920 + 0.002173.*T).*365; %Different fit using fig 2 instead
L = -0.14221.*(T.^2) + 4.31998.*T + 31.25498; %longevity with new cold data
mu = (1./L)*365;

R0 = ((EFD.*pea.*MDR.*(a^2)*b*c./(N*r1.*(mu.^2))).*exp(-mu./phi)).^(0.5);

sensDown = 0.9;
sensUp = 1.1;
aS = a.*[sensDown,sensUp];
bS = b.*[sensDown,sensUp];
cS = c.*[sensDown,sensUp];
r1S = r1.*[sensDown,sensUp];
phiS = phi.*[sensDown,sensUp];

R0a = zeros(2,size(T,2));
R0b = zeros(2,size(T,2));
R0c = zeros(2,size(T,2));
R0r1 = zeros(2,size(T,2));
R0phi = zeros(2,size(T,2));

for i = 1:2
    R0a(i,:) = ((EFD.*pea.*MDR.*(aS(i)^2)*b*c./(N*r1.*(mu.^2))).*exp(-mu./phi)).^(0.5);
    R0b(i,:) = ((EFD.*pea.*MDR.*(a^2)*bS(i)*c./(N*r1.*(mu.^2))).*exp(-mu./phi)).^(0.5);
    R0c(i,:) = ((EFD.*pea.*MDR.*(a^2)*b*cS(i)./(N*r1.*(mu.^2))).*exp(-mu./phi)).^(0.5);
    R0r1(i,:) = ((EFD.*pea.*MDR.*(a^2)*b*c./(N*r1S(i).*(mu.^2))).*exp(-mu./phi)).^(0.5);
    R0phi(i,:) = ((EFD.*pea.*MDR.*(a^2)*b*c./(N*r1.*(mu.^2))).*exp(-mu./phiS(i))).^(0.5);
end

plot(T,R0,'k','LineWidth',2)
hold on
%plot(T,R0c,'g','LineWidth',2)
plot(T,R0a(1,:),'r','LineWidth',2)
plot(T,R0b(1,:),'b','LineWidth',2)
plot(T,R0r1(1,:),'m','LineWidth',2)
plot(T,R0phi(1,:),'g','LineWidth',2)

plot(T,R0a(2,:),'r--','LineWidth',2)
plot(T,R0b(2,:),'b--','LineWidth',2)
plot(T,R0r1(2,:),'m--','LineWidth',2)
plot(T,R0phi(2,:),'g--','LineWidth',2)

plot(T,R0,'k','LineWidth',2)
xlabel('Temperature (\circC)')
ylabel('R0')
axis([15 35 10 35])
legend('R0','a','b & c','r_1','\phi','Location','NorthEast')


interval = [max(R0a(2,:)) - max(R0a(1,:)); max(R0b(2,:)) - max(R0b(1,:)); ...
            max(R0c(2,:)) - max(R0c(1,:)); -max(R0r1(2,:)) + max(R0r1(1,:)); ...
            max(R0phi(2,:)) - max(R0phi(1,:))]; 
        
%%%% alternative approach:
% Look at differentiating R0 with respect to each parameter and then see
% how R0 changes with each parameter - for fixed temp 24.672 since that is
% when R0 is highest
TF = 24.672;

peaF = 0.47192 + 0.0109.*TF;
%MDR = -0.00012.*365.*(T - 11.635).*(T - 61.28).*heaviside(T - 11.635);
MDRF = 5.286e-05.*365.*TF.*(TF - 10.02).*sqrt(34.17 - TF).*heaviside(TF - 10.02).*heaviside(34.17 - TF); %MDR as briere
EFDF = 0.0107.*365.*TF.*(TF - 13).*sqrt(30.8 - TF).*heaviside(TF - 13).*heaviside(30.8 - TF);
%mu = (-0.006036 + 0.001233.*T).*365; using longevity table
%mu = (-0.014920 + 0.002173.*T).*365; %Different fit using fig 2 instead
LF = -0.14221.*(TF.^2) + 4.31998.*TF + 31.25498; %longevity with new cold data
muF = (1./LF)*365;

aF = linspace(a*0.9,a*1.1,100);
bF = linspace(b*0.9,b*1.1,100);
cF = linspace(c*0.9,c*1.1,100);
r1F = linspace(r1*0.9,r1*1.1,100);
phiF = linspace(phi*0.9,phi*1.1,100);


daF = ((EFDF.*peaF.*MDRF.*b*c./(N*r1.*(muF.^2))).*exp(-muF./phi)).^(0.5);
dbF = 0.5.*((EFDF.*peaF.*MDRF.*(a^2).*c./(N*r1.*(muF.^2))).*exp(-muF./phi)).^(0.5).*((1./bF).^(0.5));
dcF = 0.5.*((EFDF.*peaF.*MDRF.*(a^2).*b./(N*r1.*(muF.^2))).*exp(-muF./phi)).^(0.5).*((1./cF).^(0.5));
dr1F = 0.5.*((EFDF.*peaF.*MDRF.*(a^2).*b.*c./(N.*(muF.^2))).*exp(-muF./phi)).^(0.5).*((1./r1F).^(1.5));
dphiF = 0.5.*((EFDF.*peaF.*MDRF.*(a^2).*b.*c./(N*r1.*(muF.^2)))).^(0.5).*(muF./(phiF.^2).*exp(-muF./phiF));

figure
plot(aF,daF)
figure
plot(bF,dbF)
figure
plot(cF,dcF)
figure
plot(r1F,dr1F)
figure
plot(phiF,dphiF)


da = ((EFD.*pea.*MDR.*b*c./(N*r1.*(mu.^2))).*exp(-mu./phi)).^(0.5);
db = 0.5.*((EFD.*pea.*MDR.*(a^2).*c./(N*r1.*(mu.^2))).*exp(-mu./phi)).^(0.5).*((1./b).^(0.5));
dc = 0.5.*((EFD.*pea.*MDR.*(a^2).*b./(N*r1.*(mu.^2))).*exp(-mu./phi)).^(0.5).*((1./c).^(0.5));
dr1 = -0.5.*((EFD.*pea.*MDR.*(a^2).*b.*c./(N.*(mu.^2))).*exp(-mu./phi)).^(0.5).*((1./r1).^(1.5));
dphi = 0.5.*((EFD.*pea.*MDR.*(a^2).*b.*c./(N*r1.*(mu.^2)))).^(0.5).*(mu./(phi.^2).*exp(-mu./phi));

aSens = da.*a./R0;
bSens = db.*b./R0;
cSens = dc.*c./R0;
r1Sens = dr1.*r1./R0;
phiSens = dphi.*phi./R0;

figure
plot(T,R0,'k')
hold on
plot(T,aSens,'r')
plot(T,bSens,'b')
plot(T,cSens,'g')
plot(T,r1Sens,'m')
plot(T,phiSens,'c')

%%%% Another type of plot - elasticity width interval
%These elasticities are all at temp = 24.67 when R0 is at it's maximum
elast_high = [max(R0a(1,:)),max(R0a(2,:));max(R0b(1,:)),max(R0b(2,:)); ...
            max(R0c(1,:)), max(R0c(2,:));max(R0r1(2,:)), max(R0r1(1,:)); ...
            max(R0phi(1,:)), max(R0phi(2,:))]; 
        
% also look at a smaller T = 17.5 - index number is 7501
elast_low = [R0a(1,7501),R0a(2,7501); R0b(1,7501),R0b(2,7501); ...
                R0c(1,7501),R0c(2,7501); R0r1(2,7501),R0r1(1,7501); ...
                R0phi(1,7501),R0phi(2,7501);];

plot(elast_high(1,:))

%trying with the tornado plot function
names = {'a','b','c','r_1','\phi'};
data = [a,b,c,r1,phi];

%subplot('Position',[0.07 0.09 0.42 0.85])
%[lowL,highL] = TorPlot(data,names,0.1,false,@R0Low_fun);
%subplot('Position',[0.56 0.09 0.42 0.85])
[lowH,highH] = TorPlot(data,names,0.1,false,@R0High_fun);


