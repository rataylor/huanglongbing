% Making a plot of the results of citmalrogue.m

clear
A = load('VaryRogueNewAF.txt');
x1 = 1./A(:,1);
y1 = A(:,2);
y2 = A(:,4);

[hAX,hLine1,hLine2] = plotyy(x1,y1,x1,y2);

set(hAX(1),'YLim',[300 800])
set(hAX(1),'YTick',[300:100:800])
set(hAX(2),'YLim',[500 3500])
set(hAX(2),'YTick',[500:500:3500])
set(hLine1,'LineStyle','--','LineWidth',2,'Marker', '*','MarkerSize',8);
set(hLine2,'LineStyle','--','LineWidth',2,'Marker', '*','MarkerSize',8);
xlabel('Average Time until Roguing (years)')
ylabel(hAX(1),'Peak Number of Infected Trees')
ylabel(hAX(2),'Total Number of Removed Trees')

% figure
% y3 = A(:,5);
% y4 = A(:,4);
% 
% [hAX,hLine1,hLine2] = plotyy(x1,y3,x1,y4);
% 
% set(hLine1,'LineStyle','--','LineWidth',2,'Marker', '*','MarkerSize',8);
% set(hLine2,'LineStyle','--','LineWidth',2,'Marker', '*','MarkerSize',8);
% xlabel('Average Time until Roguing (years)')
% ylabel(hAX(1),'Total Profit after 20 years')
% ylabel(hAX(2),'Total Number of Removed Trees')