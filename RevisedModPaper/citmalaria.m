% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees
% ALTERED: This is new model for the resubmission of PeerJ paper
% Changes: compartments for vector exposed period, add in exp term to hosts
% exposed too 
% New change - add in asymptomatic trees
% new change - add in step function for flush - change number of trees
% too? This has been changed to sine term for ease.
% Might need to add in constant death rate of psyllids

clear
format long
tic
% Parameter input
par = zeros(1,12);
par(1) = 0.05*365; % a
par(2) = 0.025; %0.1; b
par(3) = 0.02; %0.3; c
par(4) = 1/60; % r
par(5) = 1/7; % r_1
par(6) = 0.03631705*365; % mu
par(7) = 10/365; % EIP - number of days until psyllid becomes infectious
par(8) = 8000; % lambda
par(9) = 388; % psus - profit from a susceptible tree
par(10) = 0.4; % reduction in profit due to infection
rep = 35; % Cost of replacing a tree
discountr = 0.001; % discount factor

tau = 0.5; % host time delay
par(11) = tau;

par(12) = 1/3; % gam - the length of time asymptomatic - reduces r1 so that trees infectious are rogued sooner
% EIP = par(7); % psyllid time delay

% Initial conditions - need history of parameters since we have delay
% Initial infection starts exactly at t = 0 and not beforehand
S0 = 1001;
A0 = 0;
I0 = 0;
R0 = 0;
SV0 = 500;
EV10 = 0;
EV20 = 0;
EV30 = 0;
IV0 = 0;
Prof0 = 0;

uhist = [S0,A0,I0,R0,SV0,EV10,EV20,EV30,IV0,Prof0];
% Time solved for
tspan = [0,20];

% solver
options = ddeset('InitialY',[1000,0,1,0,500,0,0,0,0,0],'reltol', 1e-8, 'abstol', 1e-6);
sol = dde23(@citmalariafun,tau,uhist,tspan,options,par);

t = sol.x.';
y = sol.y.';

subplot('Position',[0.1 0.1 0.38 0.85]) %plotting tree solutions
plot(t,y(:,1),'LineWidth',2,'Color',[0.35 0.7 0.9],'LineStyle','-')
hold on
plot(t,y(:,2),'LineWidth',2,'Color',[0 0.6 0.5],'LineStyle',':')
hold on
plot(t,y(:,3),'LineWidth',2,'Color',[0.8 0.4 0],'LineStyle','--')
hold on
plot(t,y(:,4),'LineWidth',2,'Color',[0.8 0.6 0.7],'LineStyle','-.')
xlabel('Years')
ylabel('Trees')
axis([0 8 0 1100])
text(0.5,1050,'{\bf A}')
legend('Susceptible','Asymptomatic','Infected','Removed','Location','NorthEast')
legend('boxoff')
% line([2,5],[170,170],'LineWidth',2,'Color','b')
% text(6,170,'Susceptible','Color','b')
% line([2,5],[163,163],'LineWidth',2,'Color','r')
% text(6,163,'Infected','Color','r')
% line([2,5],[156,156],'LineWidth',2,'Color','m')
% text(6,156,'Removed','Color','m')

subplot('Position',[0.58 0.1 0.38 0.85]) % plotting psyllids
plot(t,y(:,5),'LineWidth',2,'Color',[0.35 0.7 0.9],'LineStyle','-') 
hold on
plot(t,y(:,6)+y(:,7)+y(:,8),'LineWidth',2,'Color',[0.95 0.9 0.25],'LineStyle',':')
hold on
plot(t,y(:,9),'LineWidth',2,'Color',[0.8 0.4 0],'LineStyle','--')
xlabel('Years')
ylabel('Psyllids')
axis([0 8 0 3800000])
text(0.5,3600000,'{\bf B}')
legend('Susceptible','Exposed','Infected','Location','NorthEast')
legend('boxoff')
% line([9,12],[660,660],'LineWidth',2,'Color','b')
% text(13,660,'Susceptible','Color','b')
% line([9,12],[635,635],'LineWidth',2,'Color',[0 0.6 0.298])
% text(13,635,'Exposed','Color',[0 0.6 0.298])
% line([9,12],[610,610],'LineWidth',2,'Color','r')
% text(13,610,'Infected','Color','r')


%cost of replacement
crep = exp(-discountr.*t).*rep.*y(:,4);
income = exp(-discountr.*t).*y(:,10);
prof = income - crep;
A = [t,crep,income,prof];
%B = [t,crep,income,prof];
toc
%dlmwrite('costnoint.txt',A,'precision','%.6f');
%no disease
%dlmwrite('costnodis.txt',A,'precision','%.6f');


