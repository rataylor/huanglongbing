% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees
% This file has the equations

function f = citmalinsectfun(t,y,Z,par)


% All the parameters
a = par(1);
b = par(2);
c = par(3);
r = par(4);
r_1 = par(5);
%mu = par(6);
EIP = par(7);
%lambda = par(8);
w = par(9);
m = par(10);
rho = par(11);
psus = par(12);
pinf = par(13);

%Seasonal forcing
T = 25*(1+0.3*sin(2*pi*t + w));

vac = 0;
for i = 1:20
    vac = vac + (heaviside((m/365)+i-1-t)).*heaviside(t-(i-1-0.0000001));
end
vac = 365*rho*vac ; %vaccination strategy, for m days each year

pea = 0.47192 + 0.0109*T;
MDR = 5.286e-05*365*T*(T - 10.02)*sqrt(34.17 - T) ;%*heaviside(T - 10.02)*heaviside(34.17 - T); %changed to Briere
%MDR = -0.043*(T - 11.635)*(T - 61.28)*heaviside(T - 11.635);
EFD = 0.0107*365*T*(T - 13)*sqrt(30.8 - T)*heaviside(T - 13)*heaviside(30.8 - T);
%mu = 9.215e-06*365*T*(T - 0)*sqrt(44.31 - T)*heaviside(T - 0)*heaviside(44.31 - T);
%mu = (-0.006036 + 0.001233*T)*365; %fit using table 4
%mu = (-0.014920 + 0.002173.*T).*365; %fit using fig 2
L = -0.14221*(T^2) + 4.31998*T + 31.25498; %fit using Table 4 plus freezing data
mu = (1/L)*365;
lambda =  EFD*pea*MDR/mu;


% Model equations
S = y(1);
I = y(2);
R = y(3);
SV = y(4);
EV = y(5);
IV = y(6);
N = 101;

ylag1 = Z(:,1); %first time lag for all y values
ylag2 = Z(:,2); %second time lag for all y values


f = [-(a*b/N)*IV*S + r_1*I; ...
    (a*b/N)*ylag1(6)*ylag1(1) - r_1*I; ...
    r*S + r_1*I; ...
    lambda - (a*c/N)*SV*I - mu*SV - vac*SV; ...
    (a*c/N)*SV*I - (a*c*exp(-mu*EIP)/N)*ylag2(4)*ylag2(2) - mu*EV - vac*EV; ...
    (a*c*exp(-mu*EIP)/N)*ylag2(4)*ylag2(2) - mu*IV - vac*IV; ...
    psus*S + pinf*psus*I;];

