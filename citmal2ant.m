% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees
% In this program we include antibiotic intervention on the trees
% In this program we plot two different antibiotic strategies in order to
% compare them

clear
format long

% Parameter input
par = zeros(1,11);
par(1) = 0.05*365; % a
par(2) = 0.1; % b
par(3) = 0.3; % c
par(4) = 1/60; % r
par(5) = 1/10; % r_1
par(6) = 0.03631705*365; % mu
par(7) = 10/365; % EIP - number of days until psyllid becomes infectious
par(8) = 8000; % lambda

tau = 0.5; % host time delay
EIP = par(7); % psyllid time delay

% Initial conditions - need history of parameters since we have delay
% Initial infection starts exactly at t = 0 and not beforehand
S0 = 101;
I0 = 0;
J0 = 0;
R0 = 0;
SV0 = 500;
EV0 = 0;
IV0 = 0;

uhist = [S0,I0,J0,R0,SV0,EV0,IV0];
% Time solved for
tspan = [0,20];
options = ddeset('InitialY',[100,1,0,0,500,0,0],'reltol', 1e-8, 'abstol', 1e-6);

for k = 1:2
    if k == 1
        %intervention parameters
        par(9) = 0.3; % kappa, proportion of trees treated
        par(10) = 0.6; % epsilon, the efficency of the treatment
        par(11) = 1/15; %r_2 the lifetime of a treated tree

        % solver
        sol = dde23(@citmalantifun,[tau,EIP],uhist,tspan,options,par);

        t1 = sol.x.';
        y1 = sol.y.';
    else
       %intervention parameters
        par(9) = 0.8; % kappa, proportion of trees treated
        par(10) = 0.9; % epsilon, the efficency of the treatment
        par(11) = 1/15; %r_2 the lifetime of a treated tree

        % solver
        sol = dde23(@citmalantifun,[tau,EIP],uhist,tspan,options,par);

        t2 = sol.x.';
        y2 = sol.y.'; 
    end
end

subplot('position',[0.09,0.55,0.43,0.43])
plot(t1,y1(:,1:4),'LineWidth',2)
axis([0 20 0 130])
set(gca,'XTick',[0 5 10 15 20]);
set(gca,'XTicklabel',{'';'';'';'';''});
text(1,120,'A','FontSize',12)
ylabel('Trees')

subplot('position',[0.09,0.1,0.43,0.43])
plot(t1,y1(:,5:7),'LineWidth',2)
axis([0 20 0 600])
xlabel('Years')
ylabel('Psyllids')

subplot('position',[0.55,0.55,0.43,0.43])
plot(t2,y2(:,1:4),'LineWidth',2)
set(gca,'XTick',[0 5 10 15 20]);
set(gca,'XTicklabel',{'';'';'';'';''});
set(gca,'YTick',[0 20 40 60 80 100 120]);
set(gca,'YTicklabel',{'';'';'';'';'';'';''});
axis([0 20 0 130])
text(1,120,'B','FontSize',12)

subplot('position',[0.55,0.1,0.43,0.43])
plot(t2,y2(:,5:7),'LineWidth',2)
axis([0 20 0 600])
set(gca,'YTick',[0 100 200 300 400 500 600]);
set(gca,'YTicklabel',{'';'';'';'';'';'';''});
xlabel('Years')

lab1 = find(y1(:,2)==max(y1(:,2)));
t1(lab1)
max(y1(:,2))

lab2 = find(y2(:,2)==max(y2(:,2)));
t2(lab2)
max(y2(:,2))

