% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees
% In this program we include antibiotic intervention on the trees

clear
format long

% Parameter input
par = zeros(1,14);
par(1) = 0.05*365; % a
par(2) = 0.1; % b
par(3) = 0.3; % c
par(4) = 1/60; % r
par(5) = 1/10; % r_1
par(6) = 0.03631705*365; % mu
par(7) = 10/365; % EIP - number of days until psyllid becomes infectious
par(8) = 8000; % lambda
%intervention parameters
par(9) = 0.8; % kappa, proportion of trees treated
par(10) = 0.9; % epsilon, the efficency of the treatment
par(11) = 1/15; %r_2 the lifetime of a treated tree
%costs
par(12) = 388; %profit from a susceptible tree
par(13) = 0.4; % reduction due to infection
par(14) = 30; %cost of treating 1 tree with antibiotic
rep = 50; %cost of replacing a tree
r = 0.001; %discount factor

tau = 0.5; % host time delay
EIP = par(7); % psyllid time delay

% Initial conditions - need history of parameters since we have delay
% Initial infection starts exactly at t = 0 and not beforehand
S0 = 101;
I0 = 0;
J0 = 0;
R0 = 0;
SV0 = 500;
EV0 = 0;
IV0 = 0;
CA0 = 0;
PR0 = 0;

uhist = [S0,I0,J0,R0,SV0,EV0,IV0,CA0,PR0];
% Time solved for
tspan = [0,20];

% solver
options = ddeset('InitialY',[100,1,0,0,500,0,0,0,0],'reltol', 1e-8, 'abstol', 1e-6);
sol = dde23(@citmalantifun,[tau,EIP],uhist,tspan,options,par);

t = sol.x.';
y = sol.y.';

subplot(1,2,1)
plot(t,y(:,1:4),'LineWidth',2)
xlabel('Years')
ylabel('Trees')
subplot(1,2,2)
plot(t,y(:,5:7),'LineWidth',2)
xlabel('Years')
ylabel('Psyllids')

%cost of antibiotic 
crep = exp(-r.*t).*rep.*y(:,4);
cant = exp(-r.*t).*y(:,8);
income = exp(-r.*t).*y(:,9);
prof = income - crep - cant;
B = [t,crep,cant,income,prof];

%dlmwrite('anticosteps09kap08.txt',B,'precision','%.6f');
