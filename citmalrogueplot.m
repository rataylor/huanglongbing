% Making a plot of the results of citmalrogue.m

A = load('VaryRogue.txt');
x1 = 1./A(:,1);
y1 = A(:,2);
y2 = A(:,3);

[hAX,hLine1,hLine2] = plotyy(x1,y1,x1,y2);

set(hLine1,'LineStyle','--','LineWidth',2,'Marker', '*','MarkerSize',8);
set(hLine2,'LineStyle','--','LineWidth',2,'Marker', '*','MarkerSize',8);
xlabel('Average Time until Roguing (years)')
ylabel(hAX(1),'Peak Number of Infected Trees')
ylabel(hAX(2),'Total Number of Removed Trees')

