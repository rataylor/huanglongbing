% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees
% This is a test of a new birth rate - where we simply multiply the current
% birth rate with a total vector term with delay

clear
format long

% Parameter input
par = zeros(1,11);
par(1) = 0.05*365; % a
par(2) = 0.1; % b
par(3) = 0.3; % c
par(4) = 1/60; % r
par(5) = 1/12; % r_1
%par(6) = 0.03631705*365; % mu
par(6) = 0.22*365;
par(7) = 10/365; % EIP - number of days until psyllid becomes infectious
par(8) = 8000; % lambda
par(9) = 388; % psus - profit from a susceptible tree
par(10) = 0.4; % reduction in profit due to infection
rep = 50; % Cost of replacing a tree
r = 0.001; % discount factor

tau = 0.5; % host time delay
EIP = par(7); % psyllid time delay
%EAT = 0.055; % egg to adult time delay in years

% Initial conditions - need history of parameters since we have delay
% Initial infection starts exactly at t = 0 and not beforehand
S0 = 101;
I0 = 0;
R0 = 0;
SV0 = 50000;
EV0 = 0;
IV0 = 0;
Prof0 = 0;

uhist = [S0,I0,R0,SV0,EV0,IV0,Prof0];
% Time solved for
tspan = [0,1];

% solver
options = ddeset('InitialY',[100,1,0,50000,0,0,0],'reltol', 1e-8, 'abstol', 1e-6);
%options= odeset('reltol', 1e-8, 'abstol', 1e-6);
sol = dde23(@citmaltestbfun,[tau,EIP],uhist,tspan,options,par);

t = sol.x.';
y = sol.y.';

subplot(1,2,1)
plot(t,y(:,1:3),'LineWidth',2)
xlabel('Years')
ylabel('Trees')
subplot(1,2,2)
plot(t,y(:,4:6),'LineWidth',2)
xlabel('Years')
ylabel('Psyllids')

%cost of replacement
crep = exp(-r.*t).*rep.*y(:,3);
income = exp(-r.*t).*y(:,7);
prof = income - crep;
%A = [t,crep,income,prof];
%B = [t,crep,income,prof];

%dlmwrite('costnoint.txt',A,'precision','%.6f');
%no disease
%dlmwrite('costnodis.txt',B,'precision','%.6f');


