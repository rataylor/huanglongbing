# README #

### What is this repository for? ###
This repository contains all the files necessary to run the simulations for article "Mathematical models are a powerful method to understand and control the spread of Huanglongbing" published in PeerJ.

### How do I get set up? ###

All the code runs in Matlab. The files in the upper level folder are those for the original submission. The files contained in the subfolder "RevisedModPaper" contains the files used for the model in the resubmission. That is, the final model code is in this RevisedModPaper folder.

There are a number of key files to point out:
citmalaria.m and citmalariafun.m are the main two files for running the simulations. citmalaria.m has the code for setting up parameter values, initial conditions, and for running the delay differential equations and plotting the results. citmalariafun.m contains the delay differential equations to be solved, as well as the temperature variation equation and flush equation. This produces the plots used in Figure 2 and Figure S2.1.

citmalinsect.m and citmalinsmult.m are files that run the delay differential equations but this time when insecticide is included. The first equation is for a single run of insecticide, for a single set of parameter values of number of days spraying and efficiency. citmalinsmult.m runs though the whole range of number of days spraying and efficiency in order to produce the required simulation data to produce figure 4. Both of these use citmalinsSpray2fun.m which contains the delay differential equations with insecticide. This can set to spring/autumn spraying but can be changed to summer/winter spraying. multinsplot.m plots the results of the data output from citmalinsmult.m to produce Fig 4. multinsplotSupp.m plots the results to produce supplementary figure S2.3

citmalrogue.m contains code to vary the level of roguing of trees, using the equations in citmalariafun.m, and save the output. This output is plotted using citmalrogueplot.m to produce supplementary figure S2.2.

By uncommenting lines in citmalaria.m and citmalinsect.m, you can save output on costs/profits of the disease over time and this output can be plotted using costplot.m to produce Figure 5.

R0combined.m contains the code to produce figure 3. The sensitivity analysis of the constant parameters using the function R0High_fun.m to calculate the sensitivity, using the downloaded program TorPlot.m.

### Contribution guidelines ###

Feel free to download code and use it to your heart's content. Please do not make changes to the repository as it represents the code used within a specific article.

### Who do I talk to? ###

Repository is owned by Rachel Taylor. If you have any comments or questions, please contact through bitbucket using username rataylor.