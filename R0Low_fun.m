function R0Low = R0Low_fun(v)

%%%%%% function would need changed if redo-ing. Remembering that it is
%%%%%% mu^3 on the bottom leads to changes in when low and high r0 occurs
%%%%%% with temp

T = 17.5;

pea = 0.47192 + 0.0109.*T;
%MDR = -0.00012.*365.*(T - 11.635).*(T - 61.28).*heaviside(T - 11.635);
MDR = 5.286e-05.*365.*T.*(T - 10.02).*sqrt(34.17 - T).*heaviside(T - 10.02).*heaviside(34.17 - T); %MDR as briere
EFD = 0.0107.*365.*T.*(T - 13).*sqrt(30.8 - T).*heaviside(T - 13).*heaviside(30.8 - T);
%mu = (-0.006036 + 0.001233.*T).*365; using longevity table
%mu = (-0.014920 + 0.002173.*T).*365; %Different fit using fig 2 instead
L = -0.14221.*(T.^2) + 4.31998.*T + 31.25498; %longevity with new cold data
mu = (1./L)*365;

N = 101;

a = v(1);
b = v(2);
c = v(3);
r1 = v(4);
phi = v(5);

R0Low = ((EFD.*pea.*MDR.*(a.^2).*b.*c./(N*r1.*(mu.^3))).*exp(-mu./phi)).^(0.5);