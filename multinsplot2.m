% Creates a colour plot based on the output from citmalinsmult.m
%This file takes two different text files and plots two figures together

%%%%%%%% Note that for SW spraying, I changed the roguing cost to $35 and
% the insecticide to be between 15 - 70. But for the SA one (original) it
% is still at 50 and 30-90. Thus it needs changed first.

A = load('MalinsmultSpray2.txt');
B = load('MalinsSWspray.txt');

%note that A[:,1] = m Number of Days spraying
% A[:,2] = rho Effectiveness of spray
% A[:,3] = peak number of infectives
% A[:,4] = time to reach peak (reaches within 5% of max)
% A[:,5] = total number of removed trees
% A[:,6] = profit without considering increased costs
% A[:,7] = profit with increased costs for more efficient insecticide


%Making changes for costs for A only
m = 5:30;
rho = 0.6:0.01:0.99;
n1 = size(m,2);
n2 = size(rho,2);

%what if we reduce the cost of removing trees to $35?
r = 0.001;
rep = 50;
rep2 = 35;
crep = exp(-r.*20).*rep.*A(:,5);
crep2 = exp(-r.*20).*rep2.*A(:,5);
A(:,7) = A(:,7) + crep - crep2;

%Change inscosts to vary between 15 and 70 rather than 20 to 90
oldinscost = 23.57.*rho./(1.25-rho);
newinscost = 15.075.*rho./(1.203-rho);
for i = 1:n1
    oldcost = exp(-r.*20).*20.*m(i).*oldinscost;
    newcost = exp(-r.*20).*20.*m(i)*2.*newinscost;
    A((i-1)*n2+1:i*n2,7) = A((i-1)*n2+1:i*n2,7) + oldcost.' - newcost.';
end


%in order to do a colour plot need to transform A and B into matrix format for
%just 1 response
for i = 1:n1
    C1(1:n2,i) = A((i-1)*n2+1:i*n2,3);
    D1(1:n2,i) = B((i-1)*n2+1:i*n2,3);
end

%Turn right way up for colour plot
C = flipud(C1);
D = flipud(D1);

%%%%% Plotting commands 4x4 plot with ony colour bar

ax1 = axes('Position',[0 0 1 1],'Visible','off'); %invisible axes for writing text outside plots
ax2 = axes('position',[0.08 0.55 0.38 0.4]);
axes(ax2)
%subplot('position',[0.08 0.55 0.38 0.4])
imagesc(m,rho,C)
caxis([81.222200000000001,91])
h1 = colorbar;
xlabel('Number of Days Spraying')
set(gca,'XTickLabel',{'10','20','30','40','50','60'})
ylabel('Effectiveness of Spray (%)') %note axis wrong way round here
ylabel(h1,'Peak Number of Infected Trees')
set(gca,'YTick',[0.6 0.69 0.79 0.89 0.99])
set(gca,'YTicklabel',{'0.99','0.9','0.8','0.7','0.6'})


ax3 = axes('position',[0.08 0.07 0.38 0.4]);
imagesc(m,rho,D)
caxis([81.222200000000001,91]) %scale of SA plot
h2 = colorbar;
xlabel('Number of Days Spraying')
set(gca,'XTickLabel',{'10','20','30','40','50','60'})
ylabel('Effectiveness of Spray (%)') %note axis wrong way round here
ylabel(h2,'Peak Number of Infected Trees')
set(gca,'YTick',[0.6 0.69 0.79 0.89 0.99])
set(gca,'YTicklabel',{'0.99','0.9','0.8','0.7','0.6'})

x = linspace(0,17000,size(A(:,2),1)); %colour bar for second type of plot


ax4 = axes('position',[0.58 0.55 0.38 0.4]);
for i = 1:size(A(:,2),1) %trying to plot the different rows as different colours
    col = A(i,1)/max(A(:,1)); %move from white to black depending on number of days spraying
    plot(A(i,2),A(i,7),'*','Color',[0 0.502 1]*col) %plotting effectiveness
    hold on
    plot(0.97,518000+x(i),'.','Color',[0 0.502 1]*col) %plotting colour bar
    hold on
end
axis([0.6 1 400000 540000])
xlabel('Effectiveness of Spray (%)')
ylabel('Profit')
text(0.94,519000,'10')
text(0.94,534000,'60')

x1 = linspace(0,10000,size(B(:,2),1)); %colour bar for second type of plot

ax5 = axes('position',[0.58 0.07 0.38 0.4]);
for i = 1:size(B(:,2),1) %trying to plot the different rows as different colours
    col = B(i,1)/max(B(:,1)); %move from white to black depending on number of days spraying
    plot(B(i,2),B(i,7),'*','Color',[0 0.502 1]*col) %plotting effectiveness
    hold on
    plot(0.65,340000+x1(i),'.','Color',[0 0.502 1]*col) %plotting colour bar
    hold on
end
axis([0.6 1 330000 410000])
xlabel('Effectiveness of Spray (%)')
ylabel('Profit')
text(0.66,341000,'10')
text(0.66,350000,'60')

%figure labels
axes(ax1) %set current axes
text(0.03,0.98,'{\bf A}')
text(0.52,0.98,'{\bf B}')
text(0.03,0.5, '{\bf C}')
text(0.52,0.5,'{\bf D}')
