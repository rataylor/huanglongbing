clear
%t = 0:0.001:2;
%T = 25.*(1+0.5.*sin(2.*pi.*t));
T = 10:0.001:40;

a = 0.05*365;
b = 0.1;
c = 0.3;
r1 = 1/10; %%%%%% THIS SHOULD BE 1/10!!!!
phi = 36.5; % the same as PDR = 1/EIP implies it takes 10 days for psyllid to become infectious
N = 101;

pea = 0.47192 + 0.0109.*T;
%MDR = -0.00012.*365.*(T - 11.635).*(T - 61.28).*heaviside(T - 11.635);
MDR = 5.286e-05.*365.*T.*(T - 10.02).*sqrt(34.17 - T).*heaviside(T - 10.02).*heaviside(34.17 - T); %MDR as briere
EFD = 0.0107.*365.*T.*(T - 13).*sqrt(30.8 - T).*heaviside(T - 13).*heaviside(30.8 - T);
%mu = (-0.006036 + 0.001233.*T).*365; using longevity table
%mu = (-0.014920 + 0.002173.*T).*365; %Different fit using fig 2 instead
L = -0.14221.*(T.^2) + 4.31998.*T + 31.25498; %longevity with new cold data
mu = (1./L)*365;

R0 = ((EFD.*pea.*MDR.*(a^2)*b*c./(N*r1.*(mu.^3))).*exp(-mu./phi)).^(0.5);

%changes to dMDR and dmu due to above changes of form
dPEA = 0.0109;
dMDR = (5.286e-05*365.*(T-10.02).*sqrt(34.17-T) + 5.286e-05*365.*T.*sqrt(34.17-T) + ...
            - 0.5*5.286e-05*365.*T.*(T-10.02).*(34.17 - T).^(-0.5)).*heaviside(T - 10.02).*heaviside(34.17 - T);
dEFD = (0.0107*365.*(T-13).*sqrt(30.8-T) + 0.0107*365.*T.*sqrt(30.8-T) + ...
            - 0.5*0.0107*365.*T.*(T-13).*(30.8 - T).^(-0.5)).*heaviside(T - 13).*heaviside(30.8 - T);
dmu = 365*(2*0.14221.*T - 4.31998)./((-0.14221.*(T.^2) + 4.31998.*T + 31.25498).^2);

%dR0/R0:
dR0 = (1./(2.*EFD)).*dEFD + (1./(2.*pea)).*dPEA + (1./(2.*MDR)).*dMDR - (3./(2*mu) + 1./(2*phi)).*dmu;

% OAT Sensitivity
names = {'a','b','c','r_1','\phi'};
data = [a,b,c,r1,phi];

subplot('position',[0.56,0.1,0.42,0.88])
[lowH,highH] = TorPlot(data,names,0.1,false,@R0High_fun);
text(11.2,5.25,'{\bf B}')


subplot('position',[0.08,0.1,0.42,0.88])
plot(T,dR0.*R0,'k','LineWidth',2)
hold on
plot(T,(1./(2.*EFD)).*dEFD.*R0,'b','LineWidth',2)
plot(T,(1./(2.*pea)).*dPEA.*R0,'r','LineWidth',2)
plot(T,(1./(2.*MDR)).*dMDR.*R0,'g','LineWidth',2)
plot(T, -(3./(2*mu) + 1./(2*phi)).*dmu.*R0, 'm','LineWidth',2)
line([0 40],[0 0],'LineStyle','--','Color','k')
axis([13.5 30 -2 2])
legend('R0','EFD','Pea','MDR','\mu','Location','SouthWest')
xlabel('Temperature (\circC)')
ylabel('dR0/dT')
text(28,1.8,'{\bf A}')


