% Running the malaria model with antibiotic for 4 different cases: high and
% low for both proportion and effectiveness of treatment.

% A program for solving the dynamics of the citrus greening situation when
% we use the malaria model with no adaptation to citrus greening apart from
% the change in the parameters and replacement of dead trees with new
% susceptible trees
% In this program we include antibiotic intervention on the trees

clear
format long

% Parameter input
par = zeros(1,13);
par(1) = 0.05*365; % a
par(2) = 0.1; % b
par(3) = 0.3; % c
par(4) = 1/60; % r
par(5) = 1/10; % r_1
par(6) = 0.03631705*365; % mu
par(7) = 10/365; % EIP - number of days until psyllid becomes infectious
par(8) = 8000; % lambda
%intervention parameters
par(9) = 0.8; % kappa, proportion of trees treated
par(10) = 0.9; % epsilon, the efficency of the treatment
par(11) = 1/15; %r_2 the lifetime of a treated tree
%costs
par(12) = 388; %profit from a susceptible tree
par(13) = 0.4; % reduction due to infection
cabio = 30; %cost of treating 1 tree with antibiotic
rep = 35; %cost of replacing a tree
r = 0.001; %discount factor

tau = 0.5; % host time delay
EIP = par(7); % psyllid time delay

% Initial conditions - need history of parameters since we have delay
% Initial infection starts exactly at t = 0 and not beforehand
S0 = 101;
I0 = 0;
J0 = 0;
R0 = 0;
SV0 = 500;
EV0 = 0;
IV0 = 0;
CA0 = 0;
PR0 = 0;

uhist = [S0,I0,J0,R0,SV0,EV0,IV0,CA0,PR0];
% Time solved for
tspan = [0,20];

kappa = [0.3,0.3,0.9,0.9];
eps = [0.6,0.83,0.6,0.83];

% kappa = [0.5,0.5,0.8,0.8];
% eps = [0.6,0.83,0.6,0.83];

for i = 1:4
    par(9) = kappa(i);
    par(10) = eps(i);

    % solver
    options = ddeset('InitialY',[100,1,0,0,500,0,0,0,0],'reltol', 1e-8, 'abstol', 1e-6);
    sol = dde23(@citmalantifun,[tau,EIP],uhist,tspan,options,par);

    t = sol.x.';
    y = sol.y.';
    if i == 1
        t1 = sol.x.';
        y1 = sol.y.';
    elseif i == 2
        t2 = sol.x.';
        y2 = sol.y.';
    elseif i == 3
        t3 = sol.x.';
        y3 = sol.y.';
    else
        t4 = sol.x.';
        y4 = sol.y.';
    end
        

    crep = exp(-r.*t).*rep.*y(:,4);
    cant = exp(-r.*t).*y(:,8)*cabio;
    income = exp(-r.*t).*y(:,9);
    prof = income - crep - cant;
    if i == 1
        A = [t,crep,cant,income,prof];
    elseif i == 2
        B = [t,crep,cant,income,prof];
    elseif i == 3
        C = [t,crep,cant,income,prof];
    else
        D = [t,crep,cant,income,prof];
    end
    hold on
    
end

%%%%%%%% Plotting the solution
% subplot('Position',[0.1 0.57 0.4 0.4])
% plot(t1,y1(:,1),'LineWidth',2,'Color','b')
% hold on
% plot(t1,y1(:,2),'LineWidth',2,'Color','r')
% plot(t1,y1(:,3),'LineWidth',2,'Color','c')
% plot(t1,y1(:,4),'LineWidth',2,'Color','m')
% axis([0 20 0 120])
% text(2,110,'LL')
% ylabel('Trees')
% 
% subplot('Position',[0.57 0.57 0.4 0.4])
% plot(t2,y2(:,1),'LineWidth',2,'Color','b')
% hold on
% plot(t2,y2(:,2),'LineWidth',2,'Color','r')
% plot(t2,y2(:,3),'LineWidth',2,'Color','c')
% plot(t2,y2(:,4),'LineWidth',2,'Color','m')
% axis([0 20 0 120])
% text(2,110,'LH')
% % legend({'Susceptible','Infected','Treated','Removed'},'FontSize',8,'Location','Best')
% % legend('boxoff')
% 
% 
% subplot('Position',[0.1 0.1 0.4 0.4])
% plot(t3,y3(:,1),'LineWidth',2,'Color','b')
% hold on
% plot(t3,y3(:,2),'LineWidth',2,'Color','r')
% plot(t3,y3(:,3),'LineWidth',2,'Color','c')
% plot(t3,y3(:,4),'LineWidth',2,'Color','m')
% axis([0 20 0 120])
% text(2,110,'HL')
% xlabel('Years')
% ylabel('Trees')
% 
% subplot('Position',[0.57 0.1 0.4 0.4])
% plot(t4,y4(:,1),'LineWidth',2,'Color','b')
% hold on
% plot(t4,y4(:,2),'LineWidth',2,'Color','r')
% plot(t4,y4(:,3),'LineWidth',2,'Color','c')
% plot(t4,y4(:,4),'LineWidth',2,'Color','m')
% axis([0 20 0 120])
% text(2,110,'HH')
% xlabel('Years')

%%%%%%%%%% Plotting key elements
minSus = [min(y1(:,1)),min(y2(:,1)),min(y3(:,1)),min(y4(:,1))];
maxInf = [max(y1(:,2)),max(y2(:,2)),max(y3(:,2)),max(y4(:,2))];
totalTreat = [y1(end,8),y2(end,8),y3(end,8),y4(end,8)];
totalRem = [y1(end,4),y2(end,4),y3(end,4),y4(end,4)];

figure
subplot('Position',[0.08 0.1 0.23 0.85])
plot(minSus,'*','MarkerSize',10)
axis([0.5 4.5 3.5 7])
xlabel('Treatment Plan')
ylabel('Minimum Susceptible Trees')
set(gca,'XTick',[1 2 3 4])
set(gca,'XTickLabel',{'LL','LH','HL','HH'})
subplot('Position',[0.4 0.1 0.23 0.85])
plot(totalTreat,'*','MarkerSize',10)
axis([0.5 4.5 140 175])
xlabel('Treatment Plan')
ylabel('Total Trees Treated')
set(gca,'XTick',[1 2 3 4])
set(gca,'XTickLabel',{'LL','LH','HL','HH'})
subplot('Position',[0.72 0.1 0.23 0.85])
plot(totalRem,'*','MarkerSize',10)
axis([0.5 4.5 108 124])
xlabel('Treatment Plan')
ylabel('Total Removed Trees')
set(gca,'XTick',[1 2 3 4])
set(gca,'XTickLabel',{'LL','LH','HL','HH'})


% dlmwrite('anticosteps06kap03.txt',A,'precision','%.6f');
% dlmwrite('anticosteps083kap03.txt',B,'precision','%.6f');
% dlmwrite('anticosteps06kap09.txt',C,'precision','%.6f');
% dlmwrite('anticosteps083kap09.txt',D,'precision','%.6f');

% plot(A(:,1),A(:,5),'k','LineWidth',2)
% hold on
% plot(B(:,1),B(:,5),'b','LineWidth',2)
% plot(C(:,1),C(:,5),'g','LineWidth',2)
% plot(D(:,1),D(:,5),'r','LineWidth',2)
% xlabel('Years')
% ylabel('Total Profit ($)')
% legend('LL','LH','HL','HH','Location','NorthWest')

